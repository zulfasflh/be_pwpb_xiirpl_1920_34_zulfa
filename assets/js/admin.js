$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

// pagination
// $(document).ready(function () {
//     $('#dtBasicExample').DataTable({
//       "paging": false // false to disable pagination (or any other option)
//     });
//     $('.dataTables_length').addClass('bs-select');
//   });

  // Basic example
$(document).ready(function () {
    $('#myTable').DataTable({
        ordering:false,
        lengthMenu: [[5,7,10,-1], [5,7,10, "All"]],
        createRow: function (row, data, index) {
            if(data[3].replace(/[\5,]/g, '') * 1 > 150000) {
                $('td', row).eq(5).addClass('text-success');
            }
        }
      });
  });
