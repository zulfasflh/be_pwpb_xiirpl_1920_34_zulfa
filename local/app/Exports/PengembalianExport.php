<?php

namespace App\Exports;

use App\Pengembalian;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PengembalianExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Pengembalian::all();
    }

    public function map($dataKmb): array
    {
        return [
            
            $dataKmb->id,
            $dataKmb->id_peminjaman,
            $dataKmb->nama_pegawai,
            $dataKmb->jumlah,
        ];
    }

    public function headings(): array
    {
        return [
            'ID Pengembalian',
            'ID Peminjaman',
            'Nama Pegawai',
            'Jumlah'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
