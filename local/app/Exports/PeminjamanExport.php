<?php

namespace App\Exports;

use App\Peminjaman;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PeminjamanExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Peminjaman::all();
    }

    public function map($dataPj): array
    {
        return [
            $dataPj->id,
            $dataPj->nama_pegawai,
            $dataPj->tgl_pinjam,
            $dataPj->tgl_kembali,
            $dataPj->status_pinjam,
        ];
    }

    public function headings(): array
    {
        return [
            'ID Peminjaman',
            'Nama Pegawai',
            'Tanggal Register',
            'Tanggal Pengembalian',
            'Status Pinjam'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
