<?php 

namespace App\Providers;
use App\Ruang;
use Illuminate\Support\ServiceProvider;

class RuangClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataRuang', Ruang::all());
        });
    }
}

?>