<?php 

namespace App\Providers;
use App\JenisBrg;
use Illuminate\Support\ServiceProvider;

class JenisClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataJenis', JenisBrg::all());
        });
    }
}

?>