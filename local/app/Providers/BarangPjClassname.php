<?php 

namespace App\Providers;
use App\Barang;
use Illuminate\Support\ServiceProvider;

class BarangPjClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataBrgPgw', Barang::all());
        });
    }
}

?>