<?php 

namespace App\Providers;
use App\Petugas;
use Illuminate\Support\ServiceProvider;

class PetugasClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataPtg', Petugas::all());
        });
    }
}

?>