<?php 

namespace App\Providers;
use App\DetailPinjam;
use Illuminate\Support\ServiceProvider;

class DetailPinjamClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataDtlPj', DetailPinjam::all());
        });
    }
}

?>