<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class JenisBrg extends Model
{
    protected $table = 'jenis';
    protected $fillable = [
    'id',
    'nama_jenis',
    'kode_jenis',
    'keterangan'
    ];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
}
