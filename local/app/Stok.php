<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $table = 'stok';
    protected $fillable = [
        'id_barang', 
        'jumlah',
    ];
    

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
}
