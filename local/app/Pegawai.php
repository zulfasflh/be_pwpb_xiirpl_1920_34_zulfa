<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $fillable = ['nip','username','email','alamat','user_id'];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
    
    public function peminjaman()
    {
        return $this->hasMany('App\Pegawai');
    }


    public function getGambar()
    {
        if (!$this->gambar) {
            return asset('images/default2.gif');
        }
        return asset('images/pgw/'.$this->gambar);
    }
}