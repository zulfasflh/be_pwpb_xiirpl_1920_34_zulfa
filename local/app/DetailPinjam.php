<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Check;

class DetailPinjam extends Model
{
    protected $table = 'detail_pinjam';
    protected $primarykey = 'id_dtlPj';
    protected $fillable = [
        'id_dtlPj',
        'id_barang',
        'jumlah',
    ];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
}
