<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = [
        'nama_pegawai',
        'id_barang',
        'id_pegawai',
        'tgl_pinjam',
        'tgl_kembali',
        'status_pinjam',
    ];


    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }

    // public function barang()
    // {
    //     return $this->hasMany('App\Barang');
    // }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai','id_pegawai');
    }
}
