<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PgwId extends Model
{
    protected $table = "pgw_id";
    protected $fillable = [
        "id_pegawai",
        "username"
    ];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }

    public function peminjaman()
    {
        return $this->belongsTo(Peminjaman::class);
    }
}
