<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = 'pengembalian';
    protected $fillable = [
        
        'id_peminjaman',
        'id_barang',
        'nama_pegawai',
        'jumlah'
    ];

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
}
