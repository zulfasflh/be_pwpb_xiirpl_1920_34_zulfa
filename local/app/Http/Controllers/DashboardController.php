<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Pegawai;
use App\Petugas;
use View;

class DashboardController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        $stok = Barang::sum('jumlah');
        $pgw = Pegawai::count();
        $ptg = Petugas::count();
        return View::make('dashboard')
        ->with(compact('barang'))
        ->with(compact('stok'))
        ->with(compact('pgw'))
        ->with(compact('ptg'));
    }

    public function detailBrg($id)
    {
       $dataBr = Barang::find($id);
        return view('admin.manajemen.barang.detail-brg',['dataBr' => $dataBr]);
    }

}
