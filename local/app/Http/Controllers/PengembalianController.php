<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengembalian;
use App\Peminjaman;
use PDF;
use App\Exports\PengembalianExport;
use Maatwebsite\Excel\Facades\Excel; 

class PengembalianController extends Controller
{
    public function index()
    {
        $dataKmb = Pengembalian::all();
        $x = Peminjaman::all();
        return view('admin.pengembalian', ['dataKmb' => $dataKmb, 'x' => $x]);
    }

    public function verifikasi($id)
    {
        $kembali = Peminjaman::where('id', $id)->first();
        return view('admin.verif', compact('kembali'));
    }

    public function updateKmb(Request $request,$id)
    {
        $kembali = Peminjaman::where('id', $id)->first();
        $kembali->status_pinjam = $request['status_pinjam'];
        return redirect('/peminjaman')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function delete($id)
    {
        $dataKmb = \App\Pengembalian::find($id);
        $dataKmb->delete();
        return redirect('/pengembalian')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }

    public function exportxls() 
    {
        return Excel::download(new PengembalianExport, 'Pengembalian.xlsx');
    }

    public function exportpdf()
    {
        $dataKmb = Pengembalian::all();
        $pdf = PDF::loadView('export.pengembalian.pengembalianpdf', ['dataKmb' => $dataKmb]);
        return $pdf->download('pengembalian.pdf');
    }
}
