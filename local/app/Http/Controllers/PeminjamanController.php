<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\DetailPinjam;
use App\Barang;
use App\Pegawai;
use App\Status;
use App\User;
use App\Exports\PeminjamanExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class PeminjamanController extends Controller
{
    public function index()
    {
        $dataBr = Barang::all();
        return view('admin.barang', ['dataBr' => $dataBr]);
    }

    public function vPj()
    {
        $dataPj = Peminjaman::all();
        return view('admin.peminjaman', ['dataPj' => $dataPj]);
    }

    public function PinjamPgw()
    {
        $dataPj = Peminjaman::all();
        $dataBrgPj = Barang::all();
        $dataUserPj = User::all();
        $dataPgwPj = Pegawai::all();
        return view('pegawai.peminjaman', compact('dataPj','dataBrgPj','dataUserPj','dataPgwPj'));
    }

    public function createPinjam(Request $request)
    {   
        $detail = new DetailPinjam();
        $detail->id_dtlPj = $request->input("id_dtlPj");
        $detail->id_barang = $request->input("id_barang");
        $detail->jumlah = $request->input("jumlah");
        $detail->save();

        $pinjam = new Peminjaman();
        $pinjam->id_barang = $request->input("id_barang");
        $pinjam->id_pegawai = $request->input("id_pegawai");
        $pinjam->nama_pegawai = $request->input("nama_pegawai");
        $pinjam->tgl_pinjam = $request->input("tgl_pinjam");
        $pinjam->tgl_kembali = $request->input("tgl_kembali");
        $pinjam->status_pinjam = $request->input("status_pinjam");
        $pinjam->save();
        
        return redirect('/barangpgw')->with('sukses','~ BERHASIL MEMINJAM ~');
        // return redirect('pegawai.barang', compact('detail','pinjam'))->with('sukses','~ BERHASIL MEMINJAM ~');
    }

    public function addStatus($id)
    {
        $status = Status::all();
        $dataPj = Peminjaman::all();
        $x = Peminjaman::find($id);
        return view('admin.manajemen.tambah-status', compact('status','x','dataPj'));
    }

    public function createStatus(Request $request)
    {
        $status = Status::create($request->all());
        return redirect('/peminjaman')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function detail($id)
    {
       $pj = Peminjaman::all();
       $x = Peminjaman::where('id',$id)->first();
       $y = Pegawai::all();
       $detailPj = Peminjaman::find($id);
       $b = Barang::find($id);
       return view('pegawai.peminjamans',['detailPj' => $detailPj, 'b' => $b, 'pj' => $pj, 'x' => $x, 'y' => $y]);
    }

    // public function detailPj($id)
    // {
    //    $x = Peminjaman::where('id',$id)->first();
    //    $y = Pegawai::all();
    //    $detailPj = Peminjaman::find($id);
    //    $b = Barang::find($id);
    //    return view('admin.manajemen.detail',['b' => $b, 'pj' => $pj, 'x' => $x, 'y' => $y]);
    // }

    public function detailPj($id)
    {
        $detailPj = Peminjaman::find($id);
        return view('admin.manajemen.detail-pj',['detailPj' => $detailPj]);
    }


    public function delete($id)
    {
        $dataPj = \App\Peminjaman::find($id);
        $dataPj->delete();
        return redirect('/peminjaman')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }

    public function exportxls() 
    {
        return Excel::download(new PeminjamanExport, 'Peminjaman.xlsx');
    }

    public function exportpdf()
    {
        $dataPj = Peminjaman::all();
        $pdf = PDF::loadView('export.peminjaman.peminjamanpdf', ['dataPj' => $dataPj]);
        return $pdf->download('peminjaman.pdf');
    }

    public function strukPj($id)
    {
        $x = Peminjaman::find($id);
        $pdf = PDF::loadView('export.peminjaman.struk-pinjam', ['x' => $x]);
        return $pdf->download('struk-pinjam.pdf');
    }

    public function report()
    {
        $report = Peminjaman::all();
        return view('export.peminjaman.reportAntrian', ['report' => $report]);
    }
}