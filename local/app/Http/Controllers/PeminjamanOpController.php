<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;

class PeminjamanOpController extends Controller
{
    public function index()
    {
        $dataPjOp = \App\Peminjaman::all();
        return view('operator.peminjaman',['dataPjOp' => $dataPjOp]);
    }

}
