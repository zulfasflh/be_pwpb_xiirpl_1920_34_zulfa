<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Stok;
use DB;
use File;
use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BarangController extends Controller
{
    public function index()
    {
        $dataBrg = Barang::all();
        // $dataBrg = DB::table('barang')
        //         ->join('petugas', 'petugas.id', '=', 'barang.petugas_id')
        //         ->join('ruang','ruang.id','=','barang.ruang_id')
        //         ->join('jenis','jenis.id','=', 'barang.jenis_id')
        //         ->select('petugas.username','ruang.nama_ruang','jenis.nama_jenis')
        //         ->get();
        return view('admin.manajemen.barang',['dataBrg' => $dataBrg]);
    }

    public function addBrg()
    {
        $dataBrg = Barang::all();
        return view('admin.manajemen.barang.tambah-brg', compact('dataBrg'));
    }

    public function createBrg(Request $request)
    {
        $dataBrg = Barang::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/brg/',$request->file('gambar')->getClientOriginalName());
            $dataBrg->gambar = $request->file('gambar')->getClientOriginalName();
            $dataBrg->save();
        }
        return redirect('/barang')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }
    
    public function addStok($id)
    {
        $stok = Stok::all();
        $dataBrg = Barang::all();
        $x = Barang::find($id);
        return view('admin.manajemen.barang.tambah-stok', compact('stok','x','dataBrg'));
    }

    public function createStok(Request $request)
    {
        $stok = Stok::create($request->all());
        return redirect('/barang')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function detailBrg($id)
    {
       $dataBr = Barang::find($id);
       return view('admin.manajemen.barang.detail-brg',['dataBr' => $dataBr]);
    }

    public function editBrg($id)
    {
        $dataBr = Barang::find($id);
        return view('admin.manajemen.barang.edit-Brg',['dataBr' => $dataBr]);
    }

    public function updateBrg(Request $request,$id)
    {
        $dataBr = Barang::find($id);
        $dataBr->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/brg/',$request->file('gambar')->getClientOriginalName());
            $dataBr->gambar = $request->file('gambar')->getClientOriginalName();
            $dataBr->save();
        }
        return redirect('/barang')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function delete($id)
    {
        $dataBrg = Barang::find($id);

        // hapus file
        $dataBrg = Barang::where('id',$id)->first();
        File::delete('images/brg/'.$dataBrg->gambar);
        $dataBrg->delete();
        return redirect('/barang')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }

    public function exportxls() 
    {
        return Excel::download(new BarangExport, 'Barang.xlsx');
    }

    public function exportpdf()
    {
        $dataBrg = Barang::all();
        $pdf = PDF::loadView('export.barang.barangpdf', ['dataBrg' => $dataBrg]);
        return $pdf->download('barang.pdf');
    }
}
