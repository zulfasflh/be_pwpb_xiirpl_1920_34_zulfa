<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BarangOperatorController extends Controller
{
    public function index()
    {
        $dataBrgOp = \App\Barang::all();
        return view('operator.data.barang',['dataBrgOp' => $dataBrgOp]);
    }

    public function detailBrgOp($id)
    {
       $dataBrgOp = \App\Barang::find($id);
        return view('operator.data.barang.detail-brg',['dataBrgOp' => $dataBrgOp]);
    }
}
