<?php

namespace App\Http\Controllers;
use Auth;
use App\Barang;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('index');
    }

    public function postlogin(Request $request)
    {
        // dd($request->all());
        if (Auth::attempt($request->only('username','password'))) {
            return redirect('/dashboardall');
        }
        return view('index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
