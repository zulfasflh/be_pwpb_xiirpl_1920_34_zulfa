<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruang;

class RuanganController extends Controller
{
    public function index()
    {
        $dataRuang = \App\Ruang::all();
        return view('admin.manajemen.ruangan',['dataRuang' => $dataRuang]);
    }

    public function addRuang()
    {
        $dataRuang = Ruang::all();
        return view('admin.manajemen.ruangan.tambah-ruang');
    }

    public function createRuang(Request $request)
    {
        $dataRuang = \App\Ruang::create($request->all());
        return redirect('/ruangan')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function detailRuang($id)
    {
       $dataRuang = \App\Ruang::find($id);
        return view('admin.manajemen.ruangan.detail-ruang',['dataRuang' => $dataRuang]);
    }

    public function editRuang($id)
    {
        $dataEditRuang = \App\Ruang::find($id);
        return view ('admin.manajemen.ruangan.edit-ruang',['dataEditRuang' => $dataEditRuang]);
    }

    public function updateRuang(Request $request,$id)
    {
        $dataRuang = \App\Ruang::find($id);
        $dataRuang->update($request->all());
        return redirect('/ruangan')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function delete($id)
    {
        $dataRuang = \App\Ruang::find($id);
        $dataRuang->delete();
        return redirect('/ruangan')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }
}
