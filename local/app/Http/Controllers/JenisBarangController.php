<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisBrg;

class JenisBarangController extends Controller
{
    public function index()
    {
        $dataJnsBrg = \App\JenisBrg::all();
        return view('admin.manajemen.jenis-barang',['dataJnsBrg' => $dataJnsBrg]);
    }

    public function addJnsBrg()
    {
        $dataJnsBrg = JenisBrg::all();
        return view('admin.manajemen.jenis-barang.tambah-jnsbrg');
    }

    public function createJnsBrg(Request $request)
    {
        $dataJnsBrg = \App\JenisBrg::create($request->all());
        return redirect('/jenisBrg')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function detailJnsBrg($id)
    {
       $dataJnsBrg = \App\JenisBrg::find($id);
        return view('admin.manajemen.jenis-barang.detail-jnsbrg',['dataJnsBrg' => $dataJnsBrg]);
    }

    public function editJnsBrg($id)
    {
        $dataJnsBrg = \App\JenisBrg::find($id);
        return view ('admin.manajemen.jenis-barang.edit-jnsbrg',['dataJnsBrg' => $dataJnsBrg]);
    }

    public function updateJnsBrg(Request $request,$id)
    {
        $dataJnsBrg = \App\JenisBrg::find($id);
        $dataJnsBrg->update($request->all());
        return redirect('/jenisBrg')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function delete($id)
    {
        $dataJnsBrg = \App\JenisBrg::find($id);
        $dataJnsBrg->delete();
        return redirect('/jenisBrg')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }
}
