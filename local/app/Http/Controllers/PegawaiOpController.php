<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PegawaiOpController extends Controller
{
    public function index()
    {
        $dataPgwop = \App\Pegawai::all();
        return view('operator.data.pegawai',['dataPgwop' => $dataPgwop]);
    }

    public function addPgwop()
    {
        return view('operator.data.pegawai.tambah-pgw');
    }
    
    public function createPgwop(Request $request)
    {
        // insert ke table user
        $user = new \App\User;
        $user->role = 'pegawai';
        $user->username = $request->username;
        $user->password = bcrypt('pegawai');
        $user->remember_token= str_random(60);
        $user->save();

        // insert ke table siswa
        $request->request->add(['user_id' => $user->id]);
        $dataPgwop = \App\Pegawai::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/pgw/',$request->file('gambar')->getClientOriginalName());
            $dataPgwop->gambar = $request->file('gambar')->getClientOriginalName();
            $dataPgwop->save();
        }
        return redirect('/pegawaiop')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function editPgwop($id)
    {
        $dataPgwop = \App\Pegawai::find($id);
        return view ('operator.data.pegawai.edit-pgw',['dataPgwop' => $dataPgwop]);
    }

    public function updatePgwop(Request $request,$id)
    {
        $dataPgwop = \App\Pegawai::find($id);
        $dataPgwop->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/pgw/',$request->file('gambar')->getClientOriginalName());
            $dataPgwop->gambar = $request->file('gambar')->getClientOriginalName();
            $dataPgwop->save();
        }
        return redirect('/pegawaiop')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function detailPgwop($id)
    {
       $dataPgwop = \App\Pegawai::find($id);
        return view('operator.data.pegawai.detail-pgw',['dataPgwop' => $dataPgwop]);
    }

    public function delete($id)
    {
        $dataPgwop = \App\Pegawai::find($id);
        $user = \App\User::find($id);
        $dataPgwop->delete();
        return redirect('/pegawaiop')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }
}
