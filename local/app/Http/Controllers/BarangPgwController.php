<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Peminjaman;

class BarangPgwController extends Controller
{
    public function index()
    {
        $dataBrgPgw = Barang::all();
        return view('pegawai.barang',['dataBrgPgw' => $dataBrgPgw]);
    }

    public function detailBrgPgw($id)
    {
       $dataBrgPgw = Barang::find($id);
        return view('pegawai.barang.detail-brg',['dataBrgPgw' => $dataBrgPgw]);
    }

    public function PinjamPgw()
    {
        $dataPjPgw = Peminjaman::all();
        return view('pegawai.peminjaman', ['dataPjPgw' => $dataPjPgw]);
    }
}
