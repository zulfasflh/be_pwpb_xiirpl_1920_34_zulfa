<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengembalian;

class PengembalianOpController extends Controller
{
    public function index()
    {
        $dataKmbOp = \App\Pengembalian::all();
        return view('operator.pengembalian',['dataKmbOp' => $dataKmbOp]);
    }

    public function create(Request $request)
    {   
        $kembali = new Pengembalian();
        $kembali->id_peminjaman = $request->input("id_peminjaman");
        $kembali->id_barang = $request->input("id_barang");
        $kembali->nama_pegawai = $request->input("nama_pegawai");
        $kembali->jumlah = $request->input("jumlah");
        $kembali->save();
        
        return view('operator.pengembalian', compact('kembali'))->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function kembalikan()
    {
        $dataPj = Peminjaman::all();
        return view('operator.pengembalian', ['dataPj' => $dataPj]);
    }
}
