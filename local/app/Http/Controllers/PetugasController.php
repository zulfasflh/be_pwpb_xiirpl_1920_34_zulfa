<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Petugas;
use App\User;

class PetugasController extends Controller
{
    public function index()
    {
        $dataPtg = Petugas::all();
        return view('admin.manajemen.petugas',['dataPtg' => $dataPtg]);
    }

    public function addPtg()
    {
        return view('admin.manajemen.petugas.tambah-ptg');
    }
    
    public function createPtg(Request $request)
    {
        // insert ke table user
        $user = new User;
        $user->role = 'operator';
        $user->username = $request->username;
        if(!empty($request->gambar)){
            $user->gambar = $request->gambar;
        }
        $user->password = bcrypt('operator');
        $user->remember_token= str_random(60);
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/user/',$request->file('gambar')->getClientOriginalName());
            $user->gambar = $request->file('gambar')->getClientOriginalName();
            $user->save();
        }
        $user->save();

        // insert ke table siswa
        $request->request->add(['user_id' => $user->id]);
        $dataDetailPtg = Petugas::create($request->all());

        return redirect('/petugas')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function editPetugas($id)
    {
        $dataEditPetugas = Petugas::find($id);
        return view ('admin.manajemen.petugas.edit-ptg',['dataEditPetugas' => $dataEditPetugas]);
    }

    public function updatePtg(Request $request,$id)
    {
        $dataPtg = Petugas::find($id);
        $dataPtg->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/ptg/',$request->file('gambar')->getClientOriginalName());
            $dataPtg->gambar = $request->file('gambar')->getClientOriginalName();
            $dataPtg->save();
        }
        return redirect('/petugas')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function detailPtg($id)
    {
       $x = \App\Petugas::where('id',$id)->first();
       $y = User::where('id',$x->user_id)->first('gambar');
        return view('admin.manajemen.petugas.detail-ptg',['dataDetailPtg' => $x, 'gambar' => $y->gambar]);
    }
    public function delete($id)
    {
        $dataPtg = Petugas::find($id);
        $user = User::find($id);
        $dataPtg->delete();
        return redirect('/petugas')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }
    
}
