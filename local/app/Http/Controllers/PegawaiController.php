<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\User;
use DB;
use File;

class PegawaiController extends Controller
{
    public function index()
    {
        $dataPgw = \App\Pegawai::all();
        return view('admin.manajemen.pegawai',['dataPgw' => $dataPgw]);
    }

    public function addPgw()
    {
        return view('admin.manajemen.pegawai.tambah-pgw');
    }
    
    public function createPgw(Request $request)
    {
        // insert ke table user
        $user = new User;
        $user->role = 'pegawai';
        $user->username = $request->username;
        if(!empty($request->gambar)){
            $user->gambar = $request->gambar;
        }
        $user->password = bcrypt('pegawai');
        $user->remember_token= str_random(60);
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/user/',$request->file('gambar')->getClientOriginalName());
            $user->gambar = $request->file('gambar')->getClientOriginalName();
            $user->save();
        }
        $user->save();

        // insert ke table siswa
        // dd($request->all());
        $request->request->add(['user_id' => $user->id]);
        $dataPgwadm = Pegawai::create($request->all());

        return redirect('/pegawai')->with('sukses','~ DATA BERHASIL DIINPUT ~');
    }

    public function editPgw($id)
    {
        $dataPgwadm = \App\Pegawai::find($id);
        return view ('admin.manajemen.pegawai.edit-pgw',['dataPgwadm' => $dataPgwadm]);
    }

    public function updatePgw(Request $request,$id)
    {
        $dataPgwadm = \App\Pegawai::find($id);
        $dataPgwadm->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/pgw/',$request->file('gambar')->getClientOriginalName());
            $dataPgwadm->gambar = $request->file('gambar')->getClientOriginalName();
            $dataPgwadm->save();
        }
        return redirect('/pegawai')->with('sukses','~ DATA BERHASIL DIUPDATE ~');
    }

    public function detailPgw($id)
    {
       $x = \App\Pegawai::where('id',$id)->first();
       $y = User::where('id',$x->user_id)->first('gambar');
        return view('admin.manajemen.pegawai.detail-pgw',['dataPgwadm' => $x, 'gambar' => $y->gambar]);
    }

    public function delete($id)
    {
        $dataPgw = \App\Pegawai::find($id);
        $user = \App\User::find($id);
        $dataPgw->delete();
        return redirect('/pegawai')->with('sukses','~ DATA BERHASIL DIHAPUS ~');
    }
}
