<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Check;

class Status extends Model
{
    protected $table = 'status';
    protected $fillable = [
        'id_peminjaman', 
        'status_pinjam',
    ];
    

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }
}
