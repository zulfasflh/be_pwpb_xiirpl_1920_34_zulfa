<?php

namespace App;

use App\Helpers\Check;
use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'petugas';
    protected $fillable = ['username','nama_petugas','email','created_at','updated_at','user_id'];

    // fungsi untuk relasi ke tabel barang
    public function barang()
    {
        return $this->hasMany(Barang::class);
    }

    // set koneksi hak akses db
    public function __construct(array $attributes = [])
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());
    }

    public function getGambar()
    {
        if (!$this->gambar) {
            return asset('images/default2.gif');
        }
        return asset('images/ptg/'.$this->gambar);
    }
}
