-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 06:33 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1920_xiirpl_3_34_zulfa`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addBrg` (`id_barang` CHAR(10), `id_ptg` CHAR(10), `id_ruang` CHAR(10), `id_jenis` CHAR(10), `nama` VARCHAR(50), `kondisi` VARCHAR(50), `keterangan` TEXT, `jumlah` INT(10), `tgl_register` DATE, `kode_barang` CHAR(10))  BEGIN
insert into barang values(
id_barang,id_ptg,id_ruang,id_jenis,nama,kondisi,keterangan,jumlah,tgl_register,kode_barang);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addPgw` (`id_pgw` CHAR(10), `nip` CHAR(50), `alamat` VARCHAR(255), `nama_pgw` VARCHAR(50))  begin
insert into pegawai(id_pgw, nip, alamat,nama_pgw) values
(id_pgw, nip, alamat,nama_pgw);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addRuang` (`id_ruang` CHAR(10), `nama_ruang` VARCHAR(50), `kode_ruang` VARCHAR(10), `keterangan` TEXT)  begin
insert into ruang(id_ruang, nama_ruang, kode_ruang, keterangan) values
(id_ruang, nama_ruang, kode_ruang, keterangan);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(10) NOT NULL,
  `nama_ruang` varchar(100) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` text,
  `jumlah` int(10) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama_ruang`, `nama_jenis`, `nama`, `kondisi`, `keterangan`, `jumlah`, `gambar`, `updated_at`, `created_at`) VALUES
(13, 'Ruang Tata Usaha', 'Perlengkapan Acara', 'Kain Pel', 'Baru', 'Untuk bersih bersih', 1, 'img13.jpg', '2019-09-19 18:00:07', '2019-09-20 00:31:51'),
(14, 'Ruang Tata Usaha', 'Perlengkapan Acara', 'Mouse', 'Baru', 'Bagus aja sih', 1, 'img9.jpg', '2019-09-19 18:01:56', '2019-09-20 00:44:34'),
(15, 'Ruang Tata Usaha', 'Perlengkapan Acara', 'CCTV', 'b aja', 'Bagus aja sih', 1, 'img4.jpg', '2019-09-19 18:02:10', '2019-09-20 00:57:50'),
(16, 'Ruang Kaprog RPL', 'Perlengkapan IT', 'Keyboard', 'Baru', 'Warna pink putih', 1, 'img10.jpg', '2019-09-19 18:17:45', '2019-09-20 01:17:26'),
(17, 'Ruang Olahraga', 'Perlengkapan Olahraga', 'Bola Futsal', 'Baru', 'Tolong kalau kempes dipompa ya', 1, 'ball.jpg', '2019-09-19 18:19:13', '2019-09-20 01:18:52'),
(18, 'Ruang Tata Usaha', 'Perlengkapan Acara', 'Sound System', 'Baru', 'warna hitam', 1, 'img2.jpg', '2019-09-19 18:22:51', '2019-09-20 01:20:04'),
(20, 'Ruang Kebersihan', 'Perlengkapan Kebersihan', 'Sapu', 'Baru', 'Bagus', 1, 'broom.jpg', '2019-09-19 18:27:44', '2019-09-20 01:25:32'),
(21, 'Ruang Kaprog RPL', 'Perlengkapan IT', 'Lan Adapter', 'b aja', 'Kalau sudah selesai, letakkan di kaprog lagi ok?', 5, 'img11.jpg', '2019-09-19 18:29:14', '2019-09-20 01:29:14'),
(22, 'Ruang Pendidikan', 'Perlengkapan IT', 'Kamera', 'Baru', 'Bagus', 1, 'img6.jpg', '2019-09-20 00:25:12', '2019-09-20 07:24:42');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id` int(10) NOT NULL,
  `id_barang` int(10) NOT NULL,
  `jumlah` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `tAddUpdateStok` AFTER INSERT ON `detail_pinjam` FOR EACH ROW BEGIN
UPDATE barang set jumlah=jumlah-NEW.jumlah
WHERE id_barang = NEW.id_barang;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateStok` AFTER UPDATE ON `detail_pinjam` FOR EACH ROW BEGIN
UPDATE barang
SET jumlah = jumlah + (NEW.jumlah - OLD.jumlah)
WHERE id_barang = NEW.id_barang;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(10) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(10) NOT NULL,
  `keterangan` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `nama_jenis`, `kode_jenis`, `keterangan`, `updated_at`, `created_at`) VALUES
(1, 'Perlengkapan Acara', 'JNS001', 'Untuk Acara', '2019-09-18 02:36:29', '0000-00-00 00:00:00'),
(2, 'Perlengkapan Olahraga', 'JNS002', 'Untuk Olahraga', '2019-09-17 23:13:05', '0000-00-00 00:00:00'),
(5, 'Perlengkapan Pendidikan', 'JNS003', NULL, '2019-09-19 18:16:16', '2019-09-20 01:11:55'),
(6, 'Perlengkapan Kesehatan', 'JNS004', NULL, '2019-09-19 18:16:28', '2019-09-20 01:12:10'),
(7, 'Perlengkapan Kebersihan', 'JNS005', NULL, '2019-09-19 18:16:41', '2019-09-20 01:12:32'),
(8, 'Perlengkapan IT', 'JNS006', NULL, '2019-09-19 18:16:50', '2019-09-20 01:15:56'),
(9, 'Peralatan MM', 'JNS007', NULL, '2019-09-20 20:54:43', '2019-09-21 03:54:43');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` char(10) NOT NULL,
  `nama_level` enum('admin','operator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
('1', ''),
('2', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_06_093655_create_users_table', 0),
(2, '2019_09_07_084157_create_barang_table', 0),
(3, '2019_09_07_084157_create_detail_pinjam_table', 0),
(4, '2019_09_07_084157_create_jenis_table', 0),
(5, '2019_09_07_084157_create_level_table', 0),
(6, '2019_09_07_084157_create_pegawai_table', 0),
(7, '2019_09_07_084157_create_peminjaman_table', 0),
(8, '2019_09_07_084157_create_pengembalian_table', 0),
(9, '2019_09_07_084157_create_petugas_table', 0),
(10, '2019_09_07_084157_create_ruang_table', 0),
(11, '2019_09_07_084157_create_users_table', 0),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_09_07_085010_create_barang_table', 0),
(14, '2019_09_07_085010_create_detail_pinjam_table', 0),
(15, '2019_09_07_085010_create_jenis_table', 0),
(16, '2019_09_07_085010_create_level_table', 0),
(17, '2019_09_07_085010_create_password_resets_table', 0),
(18, '2019_09_07_085010_create_pegawai_table', 0),
(19, '2019_09_07_085010_create_peminjaman_table', 0),
(20, '2019_09_07_085010_create_pengembalian_table', 0),
(21, '2019_09_07_085010_create_petugas_table', 0),
(22, '2019_09_07_085010_create_ruang_table', 0),
(23, '2019_09_07_085010_create_users_table', 0),
(24, '2019_09_07_115022_create_barang_table', 0),
(25, '2019_09_07_115022_create_detail_pinjam_table', 0),
(26, '2019_09_07_115022_create_jenis_table', 0),
(27, '2019_09_07_115022_create_level_table', 0),
(28, '2019_09_07_115022_create_password_resets_table', 0),
(29, '2019_09_07_115022_create_pegawai_table', 0),
(30, '2019_09_07_115022_create_peminjaman_table', 0),
(31, '2019_09_07_115022_create_pengembalian_table', 0),
(32, '2019_09_07_115022_create_petugas_table', 0),
(33, '2019_09_07_115022_create_ruang_table', 0),
(34, '2019_09_07_115022_create_users_table', 0),
(35, '2019_09_08_121906_create_barang_table', 0),
(36, '2019_09_08_121906_create_detail_pinjam_table', 0),
(37, '2019_09_08_121906_create_jenis_table', 0),
(38, '2019_09_08_121906_create_level_table', 0),
(39, '2019_09_08_121906_create_password_resets_table', 0),
(40, '2019_09_08_121906_create_pegawai_table', 0),
(41, '2019_09_08_121906_create_peminjaman_table', 0),
(42, '2019_09_08_121906_create_pengembalian_table', 0),
(43, '2019_09_08_121906_create_petugas_table', 0),
(44, '2019_09_08_121906_create_ruang_table', 0),
(45, '2019_09_08_121906_create_users_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  `nip` char(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `user_id`, `nip`, `username`, `email`, `alamat`, `gambar`, `updated_at`, `created_at`) VALUES
(14, 23, '0000001', 'pegawai', 'pegawai@gmail.com', 'Bekasi Barat', NULL, '2019-09-18 00:54:35', '2019-09-08 15:40:36'),
(17, 31, '0000002', 'filah', 'filah@gmail.com', 'Jl. Melinjo', NULL, '2019-09-18 00:56:42', '2019-09-18 00:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(10) NOT NULL,
  `nip` int(10) NOT NULL,
  `tgl_pinjam` datetime NOT NULL,
  `tgl_kembali` datetime NOT NULL,
  `status_pinjam` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id` int(10) NOT NULL,
  `id_peminjaman` int(10) NOT NULL,
  `pegawai_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `pengembalian`
--
DELIMITER $$
CREATE TRIGGER `tAddKembali` AFTER INSERT ON `pengembalian` FOR EACH ROW BEGIN
DELETE from peminjaman where id_peminjaman=id_peminjaman;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id`, `user_id`, `username`, `email`, `gambar`, `updated_at`, `created_at`) VALUES
(2, 26, 'zulfa', 'zulfa@mail.com', NULL, '2019-09-19 01:21:58', '2019-09-16 09:24:17'),
(3, 29, 'operator', 'op@gmail.com', NULL, '2019-09-16 09:51:20', '2019-09-16 09:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id` int(10) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id`, `nama_ruang`, `kode_ruang`, `keterangan`, `updated_at`, `created_at`) VALUES
(8, 'Ruang Tata Usaha', 'RU001', 'Di dekat masjid belok kiri sebelum gerbang masuk', '2019-09-18 18:22:18', '2019-09-19 00:35:42'),
(10, 'Ruang Kesehatan', 'RU002', 'UKS', '2019-09-18 17:53:20', '2019-09-19 00:53:20'),
(11, 'Ruang Kebersihan', 'RU003', 'Gedung A lantai 1 Ruang e6', '2019-09-18 18:23:04', '2019-09-19 01:23:04'),
(12, 'Ruang Olahraga', 'RU004', 'Gedung f lantai 5 ruang f14', '2019-09-19 18:04:23', '2019-09-20 01:04:23'),
(13, 'Ruang Pendidikan', 'RU005', 'Gedung t lantai 1 ruang t12', '2019-09-19 18:11:06', '2019-09-20 01:11:06'),
(14, 'Ruang Kaprog RPL', 'RU006', 'Gedung d lantai 3 ruang lab rpl 1', '2019-09-19 18:14:48', '2019-09-20 01:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '$2y$10$xN7XpbeEZpef9iLt4XLNduE6VS/cz0AFFCFPEoVQIa3FBo9AcIsou', 'kEh3RIG6FCJSypJONQekBxopViQBc0uNJ2e2aCtfmaFkpGXUm74GWIvo3fWh', '2019-09-06 03:54:45', '2019-09-06 03:54:45'),
(23, 'pegawai', 'pegawai', '$2y$10$RKzPLaAeZHtOCtcqR222oexZhytlKzD5Hib9A4nmMihp4ekKXVZIC', 'kCPVYKo6ZvdvvK14exmXRGEc95BMY6ATVaOsRoQTNacxbBnSSRb8dSbUeECj', '2019-09-08 08:40:36', '2019-09-08 08:40:36'),
(25, 'operator', 'zulfa', '$2y$10$YRY8XWj07SoColhI45Wn2.fTz5wN17.VJzYX2vH7rjTf4Vi3APgRS', 'X1REUtWRje1KyweoAyVbKvQKFTDjfTpPhavGp4cskYwSvfeYgeu03DKrbd3S', '2019-09-16 02:09:01', '2019-09-16 02:09:01'),
(29, 'operator', 'operator', '$2y$10$sApiiglvTBovYwXLpWSvBuFlXJNvrx3DOCNEC2a5kjK8HfuUuPJJq', 'igSbERgNT3v7JBoi7hOaTRAcXQd4XHhvfcNcaOFNRZUj8nZrKUx9MTogXWBU', '2019-09-16 02:51:20', '2019-09-16 02:51:20'),
(30, 'pegawai', 'filah', '$2y$10$25R5cVqWyoQVjBNBkZlEmuQXMpMEcaXjZd0QqVGE3vXHzd4/gxE9a', 'aUBRa5qH63flAjOqdMkdpw6rS6uCGFKNGdymGe6o82Je6cqTN0YJqEDVE8dp', '2019-09-17 02:20:29', '2019-09-17 02:20:29'),
(31, 'pegawai', 'filah', '$2y$10$FjqOxlQhkICbQ864OsIu7eX3tANjs8gsvPJJm4s1Pm2VJxxZkQ/QG', 'aMdkO7m0dAzhpqA4RCmIXZL5YbHpK2CrWonxD9jKLjtUdL6tVwJYTHsmqcrC', '2019-09-17 17:48:39', '2019-09-17 17:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `vjmlbarang`
--

CREATE TABLE `vjmlbarang` (
  `jmlBarang` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vjmlpegawai`
--

CREATE TABLE `vjmlpegawai` (
  `jmlPegawai` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vpengembalian`
--

CREATE TABLE `vpengembalian` (
  `id_pengembalian` char(10) DEFAULT NULL,
  `id_peminjaman` char(10) DEFAULT NULL,
  `id_pgw` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `id_barang` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
