<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin')->name('postlogin');
Route::get('/logout', 'AuthController@logout');

// FOR ALL USER
Route::group(['middleware' => ['auth','checkRole:admin,pegawai,operator']],function(){
    Route::get('/dashboardall', 'DashboardController@index')->name('dashboardall');
    Route::get('/detailBrgAll/{id}', 'DashboardController@detailBrg')->name('detailBrgAll');
});
// END ALL USER

// FOR PEGAWAI
Route::group(['middleware' => ['auth','checkRole:pegawai']],function(){
    Route::get('/barangpgw', 'BarangPgwController@index');
    Route::get('/detailBrgPgw/{id}', 'BarangPgwController@detailBrgPgw');
    
    // peminjaman
    Route::get('/peminjamanpgw', 'PeminjamanController@PinjamPgw')->name('peminjamanpgw');
    Route::get('/PinjamPgw/{id}', 'PeminjamanController@detail')->name('PinjamPgw');
    Route::post('/pinjam', 'PeminjamanController@createPinjam')->name('pinjam');
    Route::get('report', 'PeminjamanController@report')->name('report');
    // end peminjaman
});
// END PEGAWAI

// FOR OPERATOR 

Route::group(['middleware' => ['auth','checkRole:operator']],function(){
    Route::get('/barangop', 'BarangOperatorController@index');
    Route::get('/detailbrgop/{id}', 'BarangOperatorController@detailbrgop');
    Route::get('/pegawaiop', 'PegawaiOpController@index')->name('pegawaiop');
        // pegawai
        Route::get('/detailPgwop/{id}', 'PegawaiOpController@detailPgwop')->name('detailPgwop');
        Route::get('/editPgwop/{id}', 'PegawaiOpController@editPgwop')->name('editPgwop');
        Route::post('/updatePgwop/{id}', 'PegawaiOpController@updatePgwop')->name('updatePgwop');
        Route::get('/addPgwop', 'PegawaiOpController@addPgwop')->name('addPgwop');
        Route::post('/createPgwop', 'PegawaiOpController@createPgwop')->name('createPgwop');
        Route::get('deletepegawaiop/{id}', 'PegawaiOpController@delete')->name('deletepegawaiop');
        // akhir pegawai

    Route::get('/peminjamanop', 'PeminjamanOpController@index')->name('peminjamanop');
        // for peminjaman
        Route::get('/verifikasiPj', 'PeminjamanOpController@verifikasi')->name('verifikasiPj');
        // end peminjaman

    Route::get('/pengembalianop', 'PengembalianOpController@index')->name('pengembalianop');

        // for pengembalian
        Route::post('kembali', 'pengembalianOpController@create')->name('kembali');
        Route::get('kembalikan', 'pengembalianOpController@kembalikan')->name('kembalikan');
        // end pengembalian
});

// END OPERATOR

// FOR ADMIN
Route::group(['middleware' => ['auth','checkRole:admin']],function(){
    Route::get('/barang', 'BarangController@index')->name('barang');

        // for barang
        Route::post('/createBrg', 'BarangController@createBrg')->name('createBrg');
        Route::get('/addBrg', 'BarangController@addBrg')->name('addBrg');
        Route::post('/createStok', 'BarangController@createStok')->name('createStok');
        Route::get('/addStok/{id}', 'BarangController@addStok')->name('addStok');
        Route::get('/detailBrg/{id}', 'BarangController@detailBrg')->name('detailBrg');
        Route::get('editBrg/{id}', 'BarangController@editBrg')->name('editBrg');
        Route::post('updateBrg/{id}', 'BarangController@updateBrg')->name('updateBrg');
        Route::get('barangdelete/{id}', 'BarangController@delete')->name('delete');
        Route::get('/barangexportxls', 'BarangController@exportxls')->name('barangexportxls');
        Route::get('/barangexportpdf', 'BarangController@exportpdf')->name('barangexportpdf');
        // end barang

    Route::get('/jenisBrg', 'JenisBarangController@index')->name('jenisBrg');

        // jenis brg
        Route::post('/createJnsBrg', 'JenisBarangController@createJnsBrg')->name('createJnsBrg');
        Route::get('/addJnsBrg', 'JenisBarangController@addJnsBrg')->name('addJnsBrg');
        Route::get('deletejenisBrg/{id}', 'JenisBarangController@delete')->name('deletejenisBrg');
        Route::get('editJns/{id}', 'JenisBarangController@editJnsBrg')->name('editJns');
        Route::post('updateJnsBrg/{id}', 'JenisBarangController@updateJnsBrg')->name('updateJnsBrg');
        // akhir jns brg

    
    Route::get('/ruangan', 'RuanganController@index')->name('ruangan');
        // ruangan
        Route::post('updateRuang/{id}', 'RuanganController@updateRuang')->name('updateRuang');
        Route::get('editRuang/{id}', 'RuanganController@editRuang')->name('editRuang');
        Route::post('/createruang', 'RuanganController@createRuang')->name('createruang');
        Route::get('/addruang', 'RuanganController@addRuang')->name('addruang');
        Route::get('deleteruang/{id}', 'RuanganController@delete')->name('deleteruang');
        // akhir ruangan

    Route::get('/pegawai', 'PegawaiController@index')->name('pegawai');

        // pegawai
        Route::get('/detailPgwadm/{id}', 'PegawaiController@detailPgw')->name('detailPgwadm');
        Route::get('editPgwadm/{id}', 'PegawaiController@editPgw')->name('editPgwadm');
        Route::post('updatePgw/{id}', 'PegawaiController@updatePgw')->name('updatePgw');
        Route::get('/addPgw', 'PegawaiController@addPgw')->name('addPgw');
        Route::post('/createPgw', 'PegawaiController@createPgw')->name('createPgw');
        Route::get('deletepegawai/{id}', 'PegawaiController@delete')->name('deletepegawai');
        // akhir pegawai

    Route::get('/petugas', 'PetugasController@index')->name('petugas');

        // petugas
        Route::get('/detailPtg/{id}', 'PetugasController@detailPtg')->name('detailPtg');
        Route::get('editPetugas/{id}', 'PetugasController@editPetugas')->name('editPetugas');
        Route::post('updatePtg/{id}', 'PetugasController@updatePtg')->name('updatePtg');
        Route::get('/addPtg', 'PetugasController@addPtg')->name('addPtg');
        Route::post('/createPtg', 'PetugasController@createPtg')->name('createPtg');
        Route::get('deletepetugas/{id}', 'PetugasController@delete')->name('deletepetugas');
        // akhir petugas

    Route::get('/peminjaman', 'PeminjamanController@vPj')->name('peminjaman');

        // for peminjaman
        Route::post('/createPj', 'PeminjamanController@createPj')->name('createPj');
        Route::post('/createStatus', 'PeminjamanController@createStatus')->name('createStatus');
        Route::get('/addStatus/{id}', 'PeminjamanController@addStatus')->name('addStatus');
        Route::get('/detailPj/{id}', 'PeminjamanController@detailPj')->name('detailPj');
        Route::get('editPj/{id}', 'PeminjamanController@editPj')->name('editPj');
        Route::post('updatePj/{id}', 'PeminjamanController@updatePj')->name('updatePj');
        Route::get('deletepeminjaman/{id}', 'PeminjamanController@delete')->name('deletepeminjaman');
        Route::get('/Pjexportxls', 'PeminjamanController@exportxls')->name('Pjexportxls');
        Route::get('/Pjexportpdf', 'PeminjamanController@exportpdf')->name('Pjexportpdf');
        Route::get('/strukPj/{id}', 'PeminjamanController@strukPj')->name('strukPj');
        // end peminjaman

    Route::get('/pengembalian', 'PengembalianController@index')->name('pengembalian');

        // for pengembalian
        Route::get('/detailKmb/{id}', 'PengembalianController@detailKmb')->name('detailKmb');
        Route::get('editKmb/{id}', 'PengembalianController@editKmb')->name('editKmb');
        Route::post('updateKmb/{id}', 'PengembalianController@updateKmb')->name('updateKmb');
        Route::get('/verifikasi/{id}', 'PengembalianController@verifikasi')->name('verifikasi');
        Route::get('deletepengembalian/{id}', 'PengembalianController@delete')->name('deletepengembalian');
        Route::get('/Kmbexportxls', 'PengembalianController@exportxls')->name('Kmbexportxls');
        Route::get('/Kmbexportpdf', 'PengembalianController@exportpdf')->name('Kmbexportpdf');
        // end pengembalian


});
// END ADMINISTRATOR

Route::post('/home', 'HomeController@index')->name('home');
