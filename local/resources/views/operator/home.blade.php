<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventoryst | 2019</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <script src="../../assets/font/fontawesome-5/js/solid.js"></script>
    <script src="../../assets/font/fontawesome-5/js/fontawesome.js"></script>
    <link rel="stylesheet" href="../../assets/dataTables/datatables.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="../../assets/css/admin.css">


</head>
<body>

    <div class="wrapper" style="width:100vw;">
        <!-- SIDERBAR -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <div><img src="../../assets/img/more.png" alt="" style="margin-bottom:10px;width:60px;"></div>
                <h4 class="title-side">Operator</h4>
                <div class="logo">
                    <a href="akun/detail-akun.html"><img class="user-img" src="../../assets/img/ptg/ptg.jpg" alt=""></a>
                </div>
            </div>
            <ul class="list-unstyled components" style="margin-top:120px;">
                <li>
                    <a href="#"><i class="fas fa-home mr-2"></i>Home</a>
                </li>
                <li class="active">
                    <a href="#manageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-database mr-2"></i>Data</a>
                    <ul class="collapse list-unstyled" id="manageSubmenu">
                        <li>
                            <a href="data/barang.html"><i class="fas fa-dolly-flatbed mr-2"></i>Barang</a>
                        </li>
                        <li>
                            <a href="data/pegawai.html"><i class="fas fa-users-cog mr-2"></i>Pegawai</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="peminjaman.html"><i class="fas fa-dolly mr-2"></i>Peminjaman</a>
                </li>

                <li>
                    <a href="pengembalian.html"><i class="fas fa-dolly mr-2"></i>Pengembalian</a>
                </li>
            </ul>
        </nav>
        <!-- END SIDEBAR -->   

        <!-- CONTENT -->
            <!-- statistik -->
            <div id="content">
                    <nav class="navbar navbar-dash navbar-expand-md navbar-white">
                        <div class="container-fluid">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                            <div class="icon ml-auto">
                                    <h5>
                                    <a href="#sidebarCollapse"> <i class="fas fa-align-left mr-3"></i>
                                    </a>
                                    </h5>
                            </div>
                            </button>
                            <ul class="nav navbar-nav flex-row">
                                <li class="nav-item">
                                <div class="icon ml-auto">
                                    <h5>
                                    <a href="../../index.html"> <i class="fas fa-user-circle mr-3" data-toggle="tooltip" title="Keluar"></i>
                                    </a>
                                    </h5>
                                </div>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <div class="container-content">
                    <div class="col-md-10 page-content">
                            <h4 class="pt-5 pl-3" style="font-weight:350;margin-top: -50px; margin-bottom: 30px;">DATA STATISTIK</h4>
                            <div class="row ml-3">
                                <div class="col-sm-4 animate">
                                        <div class="card" style="width: 18rem;background-color: #fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-boxes mr-2"></i>
                                                </div>
                                                <h5 class="card-title">STOK BARANG</h5>
                                                <div class="display-4">20</div>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="col-sm-4 animate">
                                        <div class="card" style="width: 18rem;background-color: #fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-address-card"></i>
                                                </div>
                                                <h5 class="card-title">PEMINJAM</h5>
                                                <div class="display-4">20</div>
                                            </div>
                                        </div>
                                </div>
            
                                    <div class="col-sm-4 animate">
                                        <div class="card" style="width: 18rem;background-color: #fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-shopping-cart"></i>
                                                </div>
                                                <h5 class="card-title">PEMINJAMAN</h5>
                                                <div class="display-4">20</div>
                                        </div>
                                    </div>
                                </div>
                                
                                <h4 class="pt-5 pl-3" style="font-weight:350;text-align:center;">Selamat Datang Operator! Anda dapat mengolah data transaksi barang-barang yang ada di sekolah dengan bantuan Inventoryst!</h4>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- end statistik -->
         </div>

        <!-- END CONTENT -->
        
    </div>

        <!-- FOOTER -->
        <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright: Inventoryst
        </div>
        <!-- END FOOTER -->


    <script src="../../assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/dataTables/datatables.js"></script>
    <script type="text/javascript" src="../../assets/js/admin.js"></script>

    <script>
    	$(document).ready(function() {
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });

        $(document).ready(function () {
        $('#myTable').DataTable({
            ordering:false,
            lengthMenu: [[3], [3]],
            createRow: function (row, data, index) {
                if(data[3].replace(/[\5,]/g, '') * 1 > 150000) {
                    $('td', row).eq(3).addClass('text-success');
                }
            }
        });
    });

    </script>
</body>
</html>