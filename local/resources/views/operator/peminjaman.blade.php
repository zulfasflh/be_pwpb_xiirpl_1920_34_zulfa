@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DATA PEMINJAMAN</h1>
            <p class="lead">Anda dapat memantau data peminjaman di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

      <!-- TABLE PEMINJAMAN -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left:100px;">
                <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                    <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nama Pegawai</th>
                    <th scope="col">Tanggal Pinjam</th>
                    <th scope="col">Tanggal Kembali</th>
                    <th scope="col">Status Pinjam</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($dataPjOp as $row)
                
                    <tr>
                    <td>{{$row->nama_pegawai}}</td>
                    <td>{{$row->tgl_pinjam}}</td>
                    <td>{{$row->tgl_kembali}}</td>
                    <td>{{$row->status_pinjam}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
        <!-- AKHIR TABLE PEMINJAMAN -->

@endsection