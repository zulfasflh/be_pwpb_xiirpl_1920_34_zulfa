<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="../../assets/css/admin.css">
    <link rel="stylesheet" href="../../assets/font/fontawesome-5/css/all.min.css">
    <link rel="stylesheet" href="../../assets/dataTables/datatables.css">

    <title>Inventoryst</title>
  </head>
  <body style="background-color:#E9E9E9;">
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>
          <!-- <form class="form-inline my-2 my-lg-0 ml-auto">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form> -->

          <div class="icon ml-auto">
              <h5>
                   <a href="../../index.html"> <i class="fas fa-sign-out-alt mr-3" data-toggle="tooltip" title="Keluar"></i>
                   </a>
              </h5>
          </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- SIDE BAR -->
    <div class="row no-gutters mt-4">
        <div class="col-md-2 bg-light mt-2 pr-3 pt-4 container-sideadm">
            <ul class="nav flex-column ml-3 mb-5 mt-5 side-adm" style="text-transform:uppercase;">
                    <!-- <span style="border:5px #E9E9E9;margin-top:20px;">
                        <li class="nav-item">
                            <a class="nav-link active text-dark" href="dashboard.html">Dashboard</a>
                        </li>
                    </span> -->
                    
                    <li class="nav-item mt-3">
                        <a class="nav-link text-dark" href="manajemen.html">Home</a>
                    </li>
                    
                    <li class="nav-item mt-3">
                            <a class="nav-link text-dark" href="peminjaman.html">Peminjaman</a>
                    </li>
                    <li class="nav-item mt-3">
                            <a class="nav-link text-dark" href="pengembalian.html">Pengembalian</a>
                    </li>
             </ul>
        </div>

    <!-- AKHIR SIDE BAR -->

        <!-- DATA STATISTIK -->

        <div class="col-md-10">
                <h4 class="pt-5 pl-3" style="font-weight:250;">DATA STATISTIK</h4><hr>
    
                    <div class="row ml-3">
                        <div class="col-sm-4">
                                <div class="card bg-light" style="width: 18rem;">
                                    <div class="card-body">
                                        <div class="card-body-icon">
                                            <i class="fas fa-boxes mr-2"></i>
                                        </div>
                                        <h5 class="card-title">STOK BARANG</h5>
                                        <div class="display-4">20</div>
                                </div>
                            </div>
                        </div>
    
                        <div class="col-sm-4">
                                <div class="card bg-light" style="width: 18rem;">
                                    <div class="card-body">
                                        <div class="card-body-icon">
                                            <i class="fas fa-address-card"></i>
                                        </div>
                                        <h5 class="card-title">PEMINJAM</h5>
                                        <div class="display-4">20</div>
                                    </div>
                                </div>
                        </div>
    
                            <div class="col-sm-4">
                                <div class="card bg-light" style="width: 18rem;">
                                    <div class="card-body">
                                        <div class="card-body-icon">
                                            <i class="fas fa-shopping-cart"></i>
                                        </div>
                                        <h5 class="card-title">PEMINJAMAN</h5>
                                        <div class="display-4">20</div>
                                </div>
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>
        <!-- AKHIR DATA STATISTIK -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/dataTables/datatables.js"></script>


    <script type="text/javascript" src="../../assets/js/admin.js"></script>
  </body>
</html>