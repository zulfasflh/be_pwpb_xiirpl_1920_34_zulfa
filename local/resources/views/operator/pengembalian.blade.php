@extends('layouts.master')
@section('content')
      
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">PENGEMBALIAN</h1>
            <p class="lead">Anda dapat mengolah data pengembalian di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- SESSION ALERT -->
    @if(session('sukses'))
      <div class="alert alert-success" role="alert">
        {{session('sukses')}}
      </div>
    @endif
    <!-- END ALERT -->

    <!-- FORM pegawai -->

    <div class="container">
      <div class="row">
        <div class="col-md-6">
            <div class="box">

              <div class="box-body">

            <form class="modal-content animate" action="{{route('kembali')}}" method="POST">
            @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>PENGEMBALIAN</h5>
                </div>

                <label for="id_peminjaman" class="mt-4"><b>ID Peminjaman</b></label>
                  <select name="id_peminjaman" class="custom-select" id="inputGroupSelect04" style="height:50px;" required>
                        @foreach ($dataPj as $p)
                        <option value="{{ $p->id }}">{{ $p->id }}</option>
                        @endforeach
                  </select>

                  <label for="id_barang" class="mt-2"><b>ID Barang</b></label>
                  <select name="id_barang" class="custom-select" id="inputGroupSelect04" style="height:50px;" required>
                        @foreach ($dataPj as $b)
                        <option value="{{ $b->id_barang }}">{{ $b->id_barang }}</option>
                        @endforeach
                  </select>

                  <label for="nama_pegawai" class="mt-2"><b>Nama Pegawai</b></label>
                  <select name="nama_pegawai" class="custom-select" id="inputGroupSelect04" style="height:50px;" required>
                        @foreach ($dataPgw as $pgw)
                        <option value="{{ $pgw->username }}">{{ $pgw->username }}</option>
                        @endforeach
                  </select>

              <label for="jumlah" class="mt-2"><b>Jumlah</b></label>
              <input type="text" placeholder="" name="jumlah" required>
              <br>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Submit</button></a>
            </div>

            
          </form>

              </div>
          </div>
        </div>

        <div class="col-md-6" style="margin-top:-60px;">
            <div class="box">
              <div class="box-body">

                <div class="vbrg-vw">
                  <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                      <thead class="thead-dark">
                      <tr>
                      <th scope="col">Kode Barang</th>
                      <th scope="col">Nama Barang</th>
                      <th scope="col">Stok</th>
                      <th scope="col">Ruangan</th>
                      </tr>
                  </thead>
                  <tbody>
                  @foreach($dataBrg as $barang)
                      <tr>
                      <td>{{$barang->id}}</td>
                      <td>{{$barang->nama}}</td>
                      <td>{{$barang->jumlah}}</td>
                      <td>{{$barang->nama_ruang}}</td>
                      </tr>
                  @endforeach
                  </tbody>
                  </table>


                </div>
            </div>
            </div>
          </div>
        </div>
      </div>

    <!-- AKHIR FORM pegawai -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection