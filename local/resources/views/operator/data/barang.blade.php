@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DATA BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

      <!-- TABLE BARANG -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left: 100px;">
                <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                    <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Stok</th>
                    <th scope="col">Kondisi</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($dataBrgOp as $row)

                    <tr>
                    <td>{{$row->nama}}</td>
                    <td>{{$row->jumlah}}</td>
                    <td>{{$row->kondisi}}</td>
                    <td style="width:200px;"><a href="{{url('detailbrgop',$row->id)}}"><button class="btn btn-outline-dark" style="width:50px;"><i class="fas fa-search"></i></button></a>
                    </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
        <!-- AKHIR TABLE BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection