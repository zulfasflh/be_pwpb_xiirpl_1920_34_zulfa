@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">

        <!-- SESSION ALERT -->
        @if(session('sukses'))
          <div class="alert alert-success" role="alert">
            {{session('sukses')}}
          </div>
	      @endif
        <!-- END ALERT -->

            <h1 class="display-4">DATA PEGAWAI</h1>
            <p class="lead">Anda dapat mengolah data pegawai di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <div class="col-sm-2 ml-auto" style="margin-right:155px;margin-bottom:50px;margin-top:-100px;">
            <a href="{{route('addPgwop')}}"><button class="btn btn-outline-dark"><i class="far fa-plus-square"></i> Tambah Pegawai</button></a>
    </div>

      <!-- TABLE PEGAWAI -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left: 100px;">
                <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                    <thead class="thead-dark">
                    <tr>
                    <th scope="col">NIP</th>
                    <th scope="col">Nama Pegawai</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($dataPgwop as $row)

                    <tr>
                    <td>{{$row->nip}}</td>
                    <td>{{$row->username}}</td>
                    <td>{{$row->alamat}}</td>
                     <td style="width:200px;"><a href="{{url('detailPgwop',$row->id)}}"><button class="btn btn-outline-dark" style="width:50px;"><i class="fas fa-search"></i></button></a>
                     <a href="{{url('editPgwop',$row->id)}}"><button class="btn btn-outline-dark" style="width:50px;"><i class="far fa-edit"></i></button></a>  
                      <a href="{{url('deletepegawaiop',$row->id)}}" onclick="return confirm('Yakin ingin menghapus?')"><button class="btn btn-outline-dark" style="width:50px;"><i class="far fa-trash-alt"></i></button></a>
                    </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
        <!-- AKHIR TABLE PEGAWAI -->

@endsection