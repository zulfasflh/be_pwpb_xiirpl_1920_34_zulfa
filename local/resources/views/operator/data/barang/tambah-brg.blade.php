<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="../../../../assets/css/admin.css">
    <link rel="stylesheet" href="../../../../assets/font/fontawesome-5/css/all.min.css">
    <link rel="stylesheet" href="../../../../assets/dataTables/datatables.css">

    <title>Inventoryst</title>
  </head>
  <body style="background-color:#E9E9E9;">
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="../barang.html"> <button class="btn btn-outline-dark"><i class="fas fa-backward"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action=""  style="width: 700px;margin-top:-100px;">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start">
                    <h5 style="text-align:center;background-color:#fff;">TAMBAH BARANG</h5>
                </div>

              <label for="id_barang" class="mt-5"><b>ID Barang</b></label>
              <input type="text" placeholder="" name="id_barang" required>
              <br>

              <label for="id_ptg"><b>ID Petugas</b></label>
              <select class="custom-select" id="inputGroupSelect04" style="height:50px;border-radius:0%;">
                <option selected>Pilih...</option>
                <option value="1">ptg001</option>
                <option value="2">ptg002</option>
                <option value="3">ptg003</option>
              </select>
              <br>

              <label for="id_ruang"><b>ID Ruang</b></label>
              <select class="custom-select" id="inputGroupSelect04" style="height:50px;border-radius:0%;">
                    <option selected>Pilih...</option>
                    <option value="1">RU001</option>
                    <option value="2">RU002</option>
                    <option value="3">RU003</option>
              </select>
              <br>

              <label for="id_jenis"><b>ID Jenis</b></label>
              <select class="custom-select" id="inputGroupSelect04" style="height:50px;border-radius:0%;">
                    <option selected>Pilih...</option>
                    <option value="1">JNS001</option>
                    <option value="2">JNS002</option>
                    <option value="3">JNS003</option>
              </select>
              <br>

              <label for="nama_barang"><b>Nama Barang</b></label>
              <input type="text" placeholder="" name="nama_barang" required>

              <label for="kondisi_barang"><b>Kondisi Barang</b></label>
              <input type="text" placeholder="" name="kondisi_barang" required>

            <label for="keterangan_barang"><b>Keterangan Barang</b></label>
            <textarea class="form-control" id="keterangan_barang" rows="3"></textarea>

              <!-- <label for="keterangan_barang"><b>Keterangan Barang</b></label>
              <input type="text" placeholder="Keterangan Barang" name="keterangan_barang" required> -->

              <label for="jumlah_barang"><b>Jumlah Barang</b></label>
              <input type="text" placeholder="" name="jumlah_barang" required>
              
              <div class="form-group row">
                <label for="tgl_register" class="col-12 col-form-label"><b>Tanggal Register</b></label>
                <div class="col-12">
                    <input class="form-control" type="datetime-local" id="tgl_register">
                </div>
                </div>
            
                <label for="kode_barang"><b>Kode Barang</b></label>
                <input type="text" placeholder="" name="kode_barang" required>

                <div class="form-group">
                    <label for="gambar_barang"><b>Gambar Barang</b></label>
                    <input type="file" class="form-control-file btn btn-outline-dark" id="gambar_barang" aria-describedby="fileHelp">
                </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button></a>
            </div>

            <!-- <div class="container" style="background-color:#f1f1f1">
                  <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">X</button>
          </div> -->
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../../../assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="../../../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../../../assets/js/popper.min.js"></script>
    <script src="../../../../assets/js/bootstrap.min.js"></script>
    <script src="../../../../assets/dataTables/datatables.js"></script>


    <script type="text/javascript" src="../../../../assets/js/admin.js"></script>
  </body>
</html>