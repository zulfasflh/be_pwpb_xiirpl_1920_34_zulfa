@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('barangop')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL BARANG</h5>
                </div>

                <div class="col-lg">
                    <img src="{{$dataBrgOp->getGambar()}}" alt="..." class="img-thumbnail mx-auto d-block mt-3" style="width:200px;">
                </div>

                <label for="nama_barang"><b>Nama Barang</b></label>
                <input type="text" placeholder="" name="nama_barang" value="{{$dataBrgOp->nama}}" readonly>
  
                <label for="kondisi_barang"><b>Kondisi Barang</b></label>
                <input type="text" placeholder="" name="kondisi_barang" value="{{$dataBrgOp->kondisi}}" readonly>
  
              <label for="keterangan_barang"><b>Keterangan Barang</b></label>
              <textarea class="form-control" id="keterangan_barang" value="" rows="3">{{$dataBrgOp->keterangan}}</textarea>
  
                <label for="jumlah_barang"><b>Jumlah Barang</b></label>
                <input type="text" placeholder="" name="jumlah_barang" value="{{$dataBrgOp->jumlah}}" readonly>

                <label for="updated_at"><b>Tanggal Register</b></label>
                <input type="text" placeholder="" name="updated_at" value="{{$dataBrgOp->updated_at}}" readonly>
              
                  <label for="kode_barang"><b>Kode Barang</b></label>
                  <input type="text" placeholder="" name="kode_barang" value="1" readonly>

            </div>
            
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection