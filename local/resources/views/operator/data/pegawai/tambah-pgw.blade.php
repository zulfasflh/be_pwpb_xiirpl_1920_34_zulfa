@extends('layouts.master')
@section('content')
      
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('pegawaiop')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH PEGAWAI</h1>
            <p class="lead">Anda dapat mengolah data pegawai di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM pegawai -->

    <form class="modal-content animate" action="{{route('createPgwop')}}" method="POST" enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>TAMBAH PEGAWAI</h5>
                </div>

              <label for="nip" class="mt-5"><b>NIP</b></label>
              <input type="text" placeholder="" name="nip" required>
              <br>

              <label for="username"><b>Nama Pegawai</b></label>
              <input type="text" placeholder="" name="username" required>
              <br>

              <label for="email"><b>Email</b></label>
              <input type="text" placeholder="" name="email" required>
              <br>

            <label for="alamat"><b>Alamat</b></label>
            <textarea class="form-control" name="alamat" rows="3" required></textarea>

            <div class="form-group">
                <label for="gambar"><b>Profil Pegawai</b></label>
                <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp">
            </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button></a>
            </div>

            
          </form>

    <!-- AKHIR FORM pegawai -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection