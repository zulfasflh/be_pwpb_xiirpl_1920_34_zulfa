<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="../../../../assets/css/admin.css">
    <link rel="stylesheet" href="../../../../assets/font/fontawesome-5/css/all.min.css">
    <link rel="stylesheet" href="../../../../assets/dataTables/datatables.css">

    <title>Inventoryst</title>
  </head>
  <body>
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-dash navbar-expand-lg navbar-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="../../pengembalian.html"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL PENGEMBALIAN</h1>
            <p class="lead">Anda dapat mengolah data pengembalian di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL PENGEMBALIAN</h5>
                </div>

              <label for="id_kembali" class="mt-5"><b>ID Pengembalian</b></label>
              <input type="text" value="kembali001" placeholder="" name="id_pgw" required>
              <br>

              <label for="id_pinjam"><b>ID Peminjaman</b></label>
              <input type="text" value="pj001" placeholder="" name="id_pinjam" required>
              <br>

              <label for="id_pgw"><b>ID Pegawai</b></label>
              <input type="text" value="pgw002" placeholder="" name="id_pgw" required>
              <br>

              <button class="btn btn-outline-dark">CETAK</button>

            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FOOTER -->
    <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright: Inventoryst
    </div>
    <!-- END FOOTER -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../../../assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="../../../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../../../assets/js/popper.min.js"></script>
    <script src="../../../../assets/js/bootstrap.min.js"></script>
    <script src="../../../../assets/dataTables/datatables.js"></script>


    <script type="text/javascript" src="../../../../assets/js/admin.js"></script>
  </body>
</html>