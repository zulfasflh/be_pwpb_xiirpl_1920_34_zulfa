<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/fontawesome-5/css/all.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('assets/dataTables/datatables.css') }}"> -->
    
    <!-- data table -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.dataTables.css') }}">

    <title>Inventoryst | 2019</title>
  </head>
  <body>
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-dash navbar-expand-lg navbar-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>

    <!-- AKHIR NAVBAR -->

          @yield('content')

    <!-- FOOTER -->
    <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright: Inventoryst
    </div>
    <!-- END FOOTER -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <!-- <script src="{{ asset('assets/dataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/dataTables/DataTables-1.10.18/js/dataTables.bootstrap.min.js') }}"></script> -->
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>

    <script>
    	$(document).ready(function() {
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
    })
    </script>

    <script type="text/javascript" src="{{asset('assets/js/admin.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script>
    $(document).ready( function () {
    $('.myTable').DataTable();
    } );
    </script>
  </body>
</html>