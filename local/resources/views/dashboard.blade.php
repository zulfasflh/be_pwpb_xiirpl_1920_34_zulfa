<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventoryst | 2019</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
    <script src="{{ asset('assets/font/fontawesome-5/js/solid.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/font/fontawesome-5/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/dataTables/datatables.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="{{ url('assets/css/admin.css') }}">


</head>
<body>

    <div class="wrapper" style="width:100vw;">
        <!-- SIDERBAR -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4 class="title-side"><span>{{ auth()->user()->username }}</span></h4>
            </div>
            <div class="profile mr-3 animate"><img src="{{ auth()->user()->getGambar() }}" alt="..." style="height:200px;width:200px;" class="rounded ml-4 mb-2 img-thumbnail">
            </div>
            <ul class="list-unstyled components">
                <li>
                    <a href="#"><i class="fas fa-home mr-2"></i>Dashboard</a>
                </li>

                @if(auth()->user()->role == 'admin')
                
                <li class="active">
                    <a href="#manageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-database mr-2"></i>Manajemen</a>
                    <ul class="collapse list-unstyled" id="manageSubmenu">
                        <li>
                            <a href="{{url('barang')}}"><i class="fas fa-dolly-flatbed mr-2"></i>Barang</a>
                        </li>
                        
                        <li>
                            <a href="{{url('jenisBrg')}}"><i class="fas fa-dolly-flatbed mr-2"></i>Jenis Barang</a>
                        </li>
                        <li>
                            <a href="{{url('pegawai')}}"><i class="fas fa-users-cog mr-2"></i>Pegawai</a>
                        </li>
                        <li>
                            <a href="{{url('petugas')}}"><i class="fas fa-user-tie mr-2"></i>Petugas</a>
                        </li>
                        <li>
                            <a href="{{url('ruangan')}}"><i class="fas fa-door-open mr-2"></i>Ruangan</a>
                        </li>
                    </ul>
                </li>

                @endif

                @if(auth()->user()->role == 'operator')
                <li>
                    <a href="{{url('barangop')}}"><i class="fas fa-dolly-flatbed mr-2"></i>Barang</a>
                </li>
                <li>
                    <a href="{{url('pegawaiop')}}"><i class="fas fa-users-cog mr-2"></i>Pegawai</a>
                </li>
                <li>
                    <a href="{{url('peminjamanop')}}"><i class="fas fa-dolly mr-2"></i>Peminjaman</a>
                </li>
                
                <li>
                    <a href="{{url('pengembalianop')}}"><i class="fas fa-dolly mr-2"></i>Pengembalian</a>
                </li>
                @endif

                @if(auth()->user()->role == 'pegawai')
                <li>
                    <a href="{{url('barangpgw')}}"><i class="fas fa-dolly-flatbed mr-2"></i>List Barang</a>
                </li>
                <!-- <li>
                    <a href="{{url('peminjamanpgw')}}"><i class="fas fa-dolly mr-2"></i>Peminjaman</a>
                </li> -->
                @endif

                @if(auth()->user()->role == 'admin')
                <li>
                    <a href="{{url('peminjaman')}}"><i class="fas fa-dolly mr-2"></i>Peminjaman</a>
                </li>
                
                <li>
                    <a href="{{url('pengembalian')}}"><i class="fas fa-dolly mr-2"></i>Pengembalian</a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- END SIDEBAR -->   

        <!-- CONTENT -->
            <!-- statistik -->
            <div id="content">
                    <nav class="navbar navbar-dash navbar-expand-md" style="height:65px;">
                        <div class="container-fluid">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                            <div class="icon ml-auto">
                                    <h5>
                                    <a href="#sidebarCollapse"> <i class="fas fa-align-left mr-3"></i>
                                    </a>
                                    </h5>
                            </div>
                            </button>
                                <div class="icon ml-auto">
                                    <a href="{{url('logout')}}"><i style="font-size:23px;" class="fas fa-sign-out-alt" data-toggle="tooltip" title="logout"></i></a>
                                </div>
                        </div>
                    </nav>

                    <div class="container-content">
                    <div class="col-md-10 page-content">
                    @if(auth()->user()->role == 'admin')
                            <h4 class="pt-5 pl-3 statistikas" style="font-weight:350;margin-top:-50px;">DATA STATISTIK</h4>
                            <hr class="mb-4">
                            <div class="row ml-5">
                                <div class="col-sm-4 animate">
                                        <div class="card" style="background-color:#353a40;color:#fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-boxes mr-2"></i>
                                                </div>
                                                <h5 class="card-title">BARANG</h5>
                                                <div class="display-4">{{$stok}}</div>
                                        </div>
                                        <hr>
                                        <a href="{{route('barang')}}" class="small-box-footer" style="text-align:center;margin-top:-10px;margin-bottom:8px;">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
            
                                <div class="col-sm-4 animate">
                                        <div class="card" style="background-color:#353a40;color:#fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-address-card"></i>
                                                </div>
                                                <h5 class="card-title">PEGAWAI</h5>
                                                <div class="display-4">{{$pgw}}</div>
                                            </div>
                                            <hr>
                                            <a href="{{route('pegawai')}}" class="small-box-footer" style="text-align:center;margin-top:-10px;margin-bottom:8px;">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                        </div>
                                </div>
            
                                    <div class="col-sm-4 animate">
                                        <div class="card" style="background-color:#353a40;color:#fff;">
                                            <div class="card-body">
                                                <div class="card-body-icon">
                                                    <i class="fas fa-shopping-cart"></i>
                                                </div>
                                                <h5 class="card-title">PETUGAS</h5>
                                                <div class="display-4">{{$ptg}}</div>
                                        </div>
                                            <hr>
                                            <a href="{{route('petugas')}}" class="small-box-footer" style="text-align:center;margin-top:-10px;margin-bottom:8px;">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <h4 class="mt-2 pt-5 pl-3" style="font-weight:350;margin-top:-50px;">GALERI BARANG</h4>
                            <hr>
                            @endif

                            @if(auth()->user()->role == 'operator')
                            <h4 class="mt-2 pl-3" style="font-weight:350;margin-top: -50px;">GALLERY BARANG</h4>
                            <hr>
                            @endif

                            @if(auth()->user()->role == 'pegawai')
                            <h4 class="mt-2 pl-3" style="font-weight:350;margin-top: -50px;">GALERI BARANG</h4>
                            <hr>
                            @endif

                    
                            <div class="container">
                            <div class="row ml-5">
                                @foreach($barang as $b)
                                <div class="col-md-4">
                                    <div class="panel panel-default animate">
                                        <div class="panel-body">
                                            <div class="show_image"><a href="#" data-target="" data-toggle="modal"><img src="{{$b->getGambar()}}" class="img-thumbnail mx-auto d-block mt-3" style="width:300px;height:200px;"></a></div>
                                        </div>
                                        <div class="image-footer">
                                            <div class="button-footer" style="background-color:#fff;text-align:center;padding:13px;">
                                                <a class="btn btn-outline-dark btn-xs" href="{{url('detailBrgAll',$b->id)}}">
                                                <i class="fas fa-search"></i>
                                                Detail</a>

                                                @if(auth()->user()->role == 'pegawai')
                                                <a class="btn btn-outline-dark btn-xs" href="{{url('PinjamPgw', $b->id)}}">
                                                <i class="fas fa-share-square"></i> Pinjam
                                                </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>



                                @endforeach
                                </div>
                            </div>

                            <div class="row">

                            </div>

                            <h4 class="pt-5 pl-3 pb-5" style="font-weight:350;text-align:center;">Selamat Datang {{ auth()->user()->username }}! Anda dapat mengolah data barang-barang yang ada di sekolah dengan bantuan Inventoryst!</h4>
                        </div>
                    </div>
                </div>
            <!-- end statistik -->
         </div>

        <!-- END CONTENT -->
        
    </div>

        <!-- FOOTER -->
        <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright: Inventoryst
        </div>
        <!-- END FOOTER -->

    

    <script src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/dataTables/datatables.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/js/admin.js')}}"></script>

    <script>
    	$(document).ready(function() {
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });

        $(document).ready(function () {
        $('#myTable').DataTable({
            ordering:false,
            lengthMenu: [[3], [3]],
            createRow: function (row, data, index) {
                if(data[3].replace(/[\5,]/g, '') * 1 > 150000) {
                    $('td', row).eq(3).addClass('text-success');
                }
            }
        });
    });

    </script>
</body>
</html>