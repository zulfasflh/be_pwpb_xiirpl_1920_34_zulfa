<!DOCTYPE html>
<html>
<head>
<style>
body{
    font-family: Arial;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #353a40;
  color: white;
}
</style>
</head>
<body>

<h2>Data Peminjaman</h2>

<table class="table">
    <thead>
        <tr>
        
            <th>ID Peminjaman</th>
            <th>ID Barang</th>
            <th>Nama Pegawai</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    @foreach($dataKmb as $p)
    <tbody>
        <tr>

            <td>{{$p->id_peminjaman}}</td>
            <td>{{$p->id_barang}}</td>
            <td>{{$p->nama_pegawai}}</td>
            <td>{{$p->jumlah}}</td>
        </tr>
    </tbody>
    @endforeach
</table>

</body>
</html>
