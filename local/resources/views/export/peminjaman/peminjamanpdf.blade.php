<!DOCTYPE html>
<html>
<head>
<style>
body{
    font-family: Arial;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #353a40;
  color: white;
}
</style>
</head>
<body>

<h2>Data Peminjaman</h2>

<table class="table">
    <thead>
        <tr>
            <th>Nomor</th>
            <th>Nama Pegawai</th>
            <th>Tgl Pinjam</th>
            <th>Tgl Pengembalian</th>
            <th>Status Pinjam</th>
        </tr>
    </thead>
    <tbody>
    
    @foreach($dataPj as $p)

        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->nama_pegawai}}</td>
            <td>{{$p->tgl_pinjam}}</td>
            <td>{{$p->tgl_kembali}}</td>
            <td>{{$p->status_pinjam}}</td>
        </tr>
    @endforeach
    </tbody>

</table>

</body>
</html>
