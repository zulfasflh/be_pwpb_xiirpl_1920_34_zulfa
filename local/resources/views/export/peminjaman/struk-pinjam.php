<!DOCTYPE html>
<html>
<head>
	<title>Inventoryst | 2019</title>
</head>
<style>
    body{

    }

    .ins{
        color: #343a40;
        font-size: 2vw;
        margin-top: 10vh;
    }
    .table{
        width: 100vw;
        height: auto;
        background-color: #f9f9f9;
    }

    table{
        color: #343a40;
        font-family: monospace;
        font-size: 1vw;
        width: 400px;
        border-collapse: collapse;
        line-height: 1vh;
        text-align: center;
        position: relative;
        background-color: #fff;
        box-shadow: 0px 8px 15px 0px rgba(0,0,0,0.2);
        margin-left: 36vw;
    }

    th, td{
        padding: 12px;
        border-bottom: 1px solid #ddd;
    }

    th{
        
        font-family: Arial;
        height: 20px;
        background-color: #343a40;
        color: #fff;
        border-bottom: none;
    }

    th:hover{
        background-color: none;
    }

    tr:hover{
        background-color: #f9f9f9;
    }
    
</style>
<body>

		<table>

			<tr>
				<th></th>
				<th>Table Peminjaman</th>
				<th></th>
			</tr>

			<tr>
				<td>ID : </td>
				<td></td>
				<td>{{$x->id}}</td>
			</tr>

			<tr>
				<td>Nama Pegawai : </td>
				<td></td>
				<td>{{$x->nama_pegawai}}</td>
			</tr>

            <tr>
				<td>ID Barang : </td>
				<td></td>
				<td>{{$x->id_barang}}</td>
			</tr>

            <tr>
				<td>Tgl Pinjam : </td>
				<td></td>
				<td>{{$x->tgl_pinjam}}</td>
			</tr>

            <tr>
				<td>Tgl Kembali : </td>
				<td></td>
				<td>{{$x->tgl_kembali}}</td>
			</tr>

		</table>

</body>
</html>		