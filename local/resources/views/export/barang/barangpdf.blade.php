<!DOCTYPE html>
<html>
<head>
<style>
body{
    font-family: Arial;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #353a40;
  color: white;
}
</style>
</head>
<body>

<h2>Data Barang</h2>

<table class="table">
    <thead>
        <tr>
            <th>NAMA</th>
            <th>JENIS BARANG</th>
            <th>NAMA RUANG</th>
            <th>NAMA PETUGAS</th>
            <th>JUMLAH</th>
            <th>KONDISI</th>
        </tr>
    </thead>
    @foreach($dataBrg as $b)
    <tbody>
        <tr>
            <td>{{$b->nama}}</td>
            <td>{{$b->nama_jenis}}</td>
            <td>{{$b->nama_ruang}}</td>
            <td>{{$b->nama_pegawai}}</td>
            <td>{{$b->jumlah}}</td>
            <td>{{$b->kondisi}}</td>
        </tr>
    </tbody>
    @endforeach
</table>

</body>
</html>
