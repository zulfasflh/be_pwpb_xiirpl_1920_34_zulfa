<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">

    <title>Inventoryst | 2019</title>
  </head>
  <body>

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4 inv animate" style="font-weight:310;"><b>INVENTORYST</b></h1>
        <p class="lead animate"><i>WEBSITE <span>TO</span> MANAGE <span>YOUR</span> INVENTORIES .</i></p>

        <!-- Button to open the modal login form -->
        <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-outline-light start animate">GET STARTED !</button>

    <!-- The Modal Login -->
    <div id="id01" class="modal">
      <span onclick="document.getElementById('id01').style.display='none'" 
        class="close" style="color:red;" title="Close Modal">&times;</span>

      <!-- Modal Content -->
      <form class="modal-content animate" method="POST" action="{{route('postlogin')}}" style="width:400px;">
           @csrf
            <div class="container-start">
              Silakan Isi Data Anda .
            </div>

            <div class="container container-form">
              <label for="username"><b>Username</b></label>
              <input type="text" placeholder="Username" name="username" id="username" required>
              @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror

              <label for="password"><b>Password</b></label>
              <input type="password" placeholder="Password" id="password" name="password" required  style="margin-bottom:40px;">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror

              <button type="submit" class="btn btn-outline-dark" style="height:50px;">{{ __('Masuk') }}</button>
            </div>
      </form>
    </div>

        <!-- akhir modal login -->
 
      </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- container -->
    <div class="container">
      <div class="row justify-content-center animate  ">
          <div class="col-10 welcome">
                  W E L C O M E !
          </div>
      </div>


    </div>
    <!-- akhir container -->



    <!-- INVENTORIES -->

    <div class="container-fluid padding inventories">
        <div class="row text-center padding">
          <div class="col-xs-12 col-sm-6 col-md-4 animate">
            <img src="{{asset('assets/img/brg/ball.jpg')}}" style="width: 300px;">
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 animate">
            <img src="{{asset('assets/img/brg/chr.png')}}" style="width: 450px;">
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 animate">
            <img src="{{asset('assets/img/brg/broom.jpg')}}" style="width: 300px;">
          </div>
    
          <div class="container">
            <div class="row">
              <div class="col text-center">
                <a href="#inventoryst" class="btn btn-outline-dark btn-lg teks-kelola">KELOLA DATA BARANG SEKOLAH DENGAN INVENTORYST .</a>
              </div>
            </div>
        </div>
      </div>
</div>
    <!-- akhir INVENTORIES -->

    <!-- misi Inventoryst -->
    <div class="container-fluid padding teks-misi">
        <div class="row padding">
          <div class="col-md-12 col-lg-6">
            <h2><i>" Inventoryst dibuat untuk memudahkan Anda dalam mengolah data barang yang ada di sekolah. Mulai dari stok sampai kondisi terbaru dari barang. " </i></h2>
          </div>
          <div class="col-md-12 col-lg-6">
            <img src="{{ asset('assets/img/brg/laptop.png') }}" class="img-fluid img-laptop animate">
          </div>
        </div>
      </div>
    <!-- akhir misi inventoryst -->

    <!-- footer -->
    <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright:
   		 Inventoryst
	  </div>
    <!-- akhir footer -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  </body>
</html>