@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('barangpgw')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL BARANG</h5>
                </div>

                <div class="col-lg">
                    <img src="{{$dataBrgPgw->getGambar()}}" alt="..." class="img-thumbnail mx-auto d-block mt-3" style="width:200px;">
                </div>

                <label for="nama_barang"><b>Nama Barang</b></label>
                <input type="text" class="mt-3" placeholder="" name="nama_barang" value="{{$dataBrgPgw->nama}}" readonly>
  
                <label for="kondisi_barang"><b>Kondisi Barang</b></label>
                <input type="text" placeholder="" name="kondisi_barang" value="{{$dataBrgPgw->kondisi}}" readonly>
  
              <label for="keterangan_barang"><b>Keterangan Barang</b></label>
              <textarea class="form-control" id="keterangan_barang" value="" rows="3">{{$dataBrgPgw->keterangan}}</textarea>
  
                <label for="jumlah_barang"><b>Jumlah Barang</b></label>
                <input type="text" placeholder="" name="jumlah_barang" value="{{$dataBrgPgw->jumlah}}" readonly>

            </div>

            <div class="container" style="background-color:#f1f1f1">
                <a href="#"><button class="btn btn-outline-dark">Pinjam Barang</button></a>
          </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->
@endsection