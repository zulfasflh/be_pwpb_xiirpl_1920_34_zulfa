@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
        <!-- SESSION ALERT -->
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        <!-- END ALERT -->
            <h1 class="display-4">DATA BARANG</h1>
            <hr>
        </div>
    </div>
      <!-- TABLE BARANG -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left: 100px;">
                <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                    <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Stok</th>
                    <th scope="col">Kondisi</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($dataBrgPgw as $data)

                    <tr>
                    <td>{{$data->nama}}</td>
                    <td>{{$data->jumlah}}</td>
                    <td>{{$data->kondisi}}</td>
                    <td style="width:200px;">
                    <!-- <a href="{{url('detailBrgPgw',$data->id)}}"><button class="btn btn-outline-dark" style="width:50px;"><i class="fas fa-search"></i></button></a> -->
                    <a href="{{url('PinjamPgw',$data->id)}}"><button class="btn btn-outline-dark ml-1" style="width:50px;"><i class="fas fa-share-square"></i></button></a>
                    </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
        <!-- AKHIR TABLE BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection