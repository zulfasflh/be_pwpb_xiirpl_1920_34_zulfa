@extends('layouts.master')
@section('content')
      
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{url('barangpgw')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">PEMINJAMAN</h1>
            <p class="lead">Anda dapat mengolah data peminjaman di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM pegawai -->

    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <div class="box">

              <div class="box-body">

            <form class="modal-content animate" action="{{route('pinjam')}}" method="POST">
            @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>PEMINJAMAN</h5>
                </div>

                <label class="mt-4" for="nama_pegawai"><b>Nama Pegawai</b></label>
                <input class="form-control" type="text" placeholder="" name="nama_pegawai" style="height:50px;" value="{{ auth()->user()->username }}" readonly>

                <label for="id_pegawai"><b>ID Pegawai</b></label>
                <select class="form-control mt-1" name="id_pegawai" id="id_pegawai">
                    @foreach($y as $p)
                    <option>{{$p->id}}</option>
                    @endforeach
                </select>

                <label class="mt-3" for="tgl_pinjam"><b>Tanggal Pinjam</b></label>
                  <input class="form-control" type="date" placeholder="" name="tgl_pinjam" style="height:50px;" value="{{date('Y-m-d')}}" readonly>
                
                <label class="mt-3" for="tgl_kembali"><b>Tanggal Kembali</b></label>
                  <input class="form-control" type="date" min="{{date('Y-m-d')}}" max="{{'Y-m-d', time() + (60 * 60 * 24 * 5)}}" placeholder="" name="tgl_kembali" style="height:50px;" required>
                
                <label class="mt-3" for="status_pinjam"><b>Status</b></label>
                  <input type="text" name="status_pinjam" id="status_pinjam" class="form-control" value="Antrian" style="height:50px;" readonly>
                
                <!-- detail-pinjam -->

                <label class="mt-4" for="id_barang"><b>Kode Barang</b></label>
                <input class="form-control" type="text" placeholder="" name="id_barang" style="height:50px;" value="{{ $b->id }}" readonly>

                <label class="mt-2" for="jumlah"><b>Jumlah</b></label>
                  <input name="jumlah" type="number" min="1" max="{{ $b->jumlah }}" id="jumlah" class="form-control" style="height:50px;" required>
                <br>
                
              @if($b->jumlah == 0) 
                <a href="#"><button title="STOK TIDAK ADA" type="submit" class="btn btn btn-danger" style="height:50px;">~ STOK TIDAK ADA ~</button></a>
              @else
                <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Pinjam</button></a>
              @endif
              <!-- <a href="{{route('report')}}"><button class="btn btn-outline-dark"><i class="fas fa-file-pdf"></i> Export PDF </button></a> -->
            </div>

            
          </form>

              </div>
          </div>
        </div>

            </div>
            </div>
          </div>
        </div>
      </div>

    <!-- AKHIR FORM pegawai -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection
@push('script')
$("[type='number']").keypress(function (evt) {
  evt.preventDefault();
});
@endpush