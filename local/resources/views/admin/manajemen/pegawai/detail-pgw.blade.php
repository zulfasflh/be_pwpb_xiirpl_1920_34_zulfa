@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('pegawai')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL PEGAWAI</h1>
            <p class="lead">Anda dapat mengolah data pegawai di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL PEGAWAI</h5>
                </div>

                <div class="col-lg">
                    <img src="{{  asset('images/user/'.$gambar) }}" alt="..." class="img-thumbnail mx-auto d-block mt-3" style="width:200px;">
                </div>

                <label for="id_pgw" class="mt-5"><b>ID Pegawai</b></label>
              <input type="text" value="{{$dataPgwadm->id}}" placeholder="" name="id_pgw" readonly>

              <label for="nip"><b>NIP</b></label>
              <input type="text" value="{{$dataPgwadm->nip}}" placeholder="" name="nip" readonly>
              <br>

              <label for="username"><b>Nama Pegawai</b></label>
              <input type="text" value="{{$dataPgwadm->username}}" placeholder="" name="username" readonly>

              <label for="email"><b>Email</b></label>
              <input type="text" value="{{$dataPgwadm->email}}" placeholder="" name="email" readonly>

            <label for="alamat"><b>Alamat</b></label>
            <textarea class="form-control" id="alamat" rows="3" readonly>{{$dataPgwadm->alamat}}</textarea>

            </div>

            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->
@endsection