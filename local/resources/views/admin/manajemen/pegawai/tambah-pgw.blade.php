<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/fontawesome-5/css/all.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('assets/dataTables/datatables.css') }}"> -->

    <title>Inventoryst</title>
  </head>
  <body>
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-dash navbar-expand-lg navbar-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>
      
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('pegawai')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH PEGAWAI</h1>
            <p class="lead">Anda dapat mengolah data pegawai di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM pegawai -->

    <form class="modal-content animate" action="{{Route('createPgw')}}" method="POST" enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>TAMBAH PEGAWAI</h5>
                </div>

              <label for="nip" class="mt-5"><b>NIP</b></label>
              <input type="text" placeholder="" name="nip" required>
              <br>

              <label for="username"><b>Nama Pegawai</b></label>
              <input type="text" placeholder="" name="username" required>
              <br>

              <label for="email"><b>Email</b></label>
              <input type="text" placeholder="" name="email" required>
              <br>

            <label for="alamat"><b>Alamat</b></label>
            <textarea class="form-control" name="alamat" rows="3" required></textarea>

            <div class="form-group">
                <label for="gambar"><b>Profil Pegawai</b></label>
                <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp">
            </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button></a>
            </div>

            
          </form>

    <!-- AKHIR FORM pegawai -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FOOTER -->
    <div class="footer-copyright text-center py-3 pt-5">© 2019 Copyright: Inventoryst
    </div>
    <!-- END FOOTER -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('assets/js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{asset('assets/js/admin.js')}}"></script>
  </body>
</html>