@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{Route('pegawai')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">EDIT PEGAWAI</h1>
            <p class="lead">Anda dapat mengolah data pegawai di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="{{route('updatePgw',$dataPgwadm->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>EDIT PEGAWAI</h5>
              </div>

              <label for="id" class="mt-5"><b>ID Pegawai</b></label>
              <input type="text" value="{{$dataPgwadm->id}}" placeholder="" name="id" required>

              <label for="nip"><b>NIP</b></label>
              <input type="text" value="{{$dataPgwadm->nip}}" placeholder="" name="nip" required>
              <br>

              <label for="username"><b>Nama Pegawai</b></label>
              <input type="text" value="{{$dataPgwadm->username}}" placeholder="" name="username" required>

              <label for="email"><b>Email</b></label>
              <input type="text" value="{{$dataPgwadm->email}}" placeholder="" name="email" required>

            <label for="alamat"><b>Alamat</b></label>
            <textarea class="form-control" name="alamat" id="alamat" rows="3">{{$dataPgwadm->alamat}}</textarea>

            <div class="form-group">
                <label for="gambar"><b>Profil Pegawai</b></label>
                <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp" value="{{$dataPgwadm->gambar}}">
            </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Update</button></a>
            </div>
            
          </form>

    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection