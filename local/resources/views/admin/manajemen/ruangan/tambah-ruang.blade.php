@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('ruangan')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH RUANGAN</h1>
            <p class="lead">Anda dapat mengolah data ruangan di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->

    <form class="modal-content animate" action="{{route('createruang')}}" method="POST">
    @csrf
        <div class="container-start anda mt-2">
          <!-- Silakan Isi Data Anda . -->
        </div>

        <div class="container container-form">
            <div class="container-start container-vw">
                <h5>TAMBAH RUANG</h5>
            </div>

          <label for="nama_ruang" class="mt-5"><b>Nama Ruang</b></label>
          <input type="text" placeholder="" name="nama_ruang" required>
        
            <label for="kode_ruang"><b>Kode Ruang</b></label>
            <input type="text" placeholder="" name="kode_ruang" required>

        <label for="keterangan"><b>Keterangan Ruangan</b></label>
        <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>

          <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Submit</button></a>
        </div>

      </div>
        
      </form>

    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection