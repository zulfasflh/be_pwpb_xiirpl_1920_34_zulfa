@extends('layouts.master')
@section('content')
        <div class="row ml-auto">
            <div class="col-lg">
                <div class="icon ml-3">
                    <h5>
                        <a href="{{route('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                        </a>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">

            <!-- SESSION ALERT -->
            @if(session('sukses'))
              <div class="alert alert-success" role="alert">
                {{session('sukses')}}
              </div>
            @endif
            <!-- END ALERT -->

            <h1 class="display-4">DATA BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->
    <div class="col-sm-2 ml-auto" style="margin-right:155px;margin-bottom:50px;margin-top:-100px;">
        <a href="{{route('addBrg')}}"><button class="btn btn-outline-dark"><i class="far fa-plus-square"></i> Tambah Barang</button></a>
    </div>

    <div class="col-sm-2 ml-auto" style="margin-right:355px;margin-bottom:50px;margin-top:-104px;">
      <a href="{{route('barangexportxls')}}"><button class="btn btn-outline-dark"><i class="fas fa-file-excel"></i> Export Excel </button></a>
    </div>

    <div class="col-sm-2 ml-auto" style="margin-right:555px;margin-bottom:50px;margin-top:-105px;">
      <a href="{{route('barangexportpdf')}}"><button class="btn btn-outline-dark"><i class="fas fa-file-pdf"></i> Export PDF </button></a>
    </div>

      <!-- TABLE BARANG -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left: 100px;">
                <table class="myTable table shadow-sm p-3 mb-5 bg-white rounded">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">ID Barang</th>
                    <th scope="col">Stok</th>
                    <th scope="col">Kondisi</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($dataBrg as $barang)
                
                    <tr>
                      <td>{{$barang->nama}}</td>
                      <td>{{$barang->id}}</td>
                      <td>{{$barang->jumlah}}</td>
                      <td>{{$barang->kondisi}}</td>
                      <td>{{$barang->keterangan}}</td>
                      <td style="width:200px;">
                            <a href="{{route('addStok',$barang->id)}}">
                            <button class="btn btn-outline-dark" style="width:40px;"><i class="far fa-plus-square"></i></button></a>

                            <a href="{{url('detailBrg',$barang->id)}}">
                            <button class="btn btn-outline-dark" style="width:40px;"><i class="fas fa-search"></i></button></a> 

                            <a href="{{url('editBrg',$barang->id)}}"><button class="btn btn-outline-dark" style="width:40px;"><i class="far fa-edit"></i></button></a>  

                            <a href="{{url('barangdelete',$barang->id)}}" onclick="return confirm('Yakin ingin menghapus?')"><button class="btn btn-outline-dark" style="width:40px;"><i class="far fa-trash-alt"></i></button></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
        <!-- AKHIR TABLE BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection