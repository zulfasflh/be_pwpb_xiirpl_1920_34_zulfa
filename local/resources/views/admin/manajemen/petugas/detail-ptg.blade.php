@extends('layouts.master')
@section('content')
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('petugas')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->

    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL PETUGAS</h1>
            <p class="lead">Anda dapat mengolah data petugas di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL PETUGAS</h5>
                </div>

                <div class="col-lg">
                    <img src="{{  asset('images/user/'.$gambar) }}" alt="..." class="img-thumbnail mx-auto d-block mt-3" style="width:200px;">
                </div>

                <label for="id" class="mt-5"><b>ID Petugas</b></label>
                <input type="text" value="{{$dataDetailPtg->id}}" placeholder="" name="id" readonly>
                <br>
  
                <label for="username"><b>Username</b></label>
                <input type="text" value="{{$dataDetailPtg->username}}" placeholder="" name="username" readonly>
                <br>

                <label for="nama_petugas"><b>Nama Petugas</b></label>
                <input type="text" value="{{$dataDetailPtg->nama_petugas}}" placeholder="" name="nama_petugas" readonly>
                <br>
                
                <label for="email"><b>Email</b></label>
                <input type="text" value="{{$dataDetailPtg->email}}" placeholder="" name="email" readonly>
                <br>
  
            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->
@endsection