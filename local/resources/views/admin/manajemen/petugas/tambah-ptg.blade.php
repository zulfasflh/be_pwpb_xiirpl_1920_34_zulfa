@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('petugas')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH PETUGAS</h1>
            <p class="lead">Anda dapat mengolah data petugas di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM pegawai -->

    <form class="modal-content animate" action="{{route('createPtg')}}" method="POST" enctype="multipart/form-data">
      @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>TAMBAH PETUGAS</h5>
                </div>
              
              <label for="username" class="mt-4"><b>Username</b></label>
              <input type="text" value="" placeholder="" name="username" required>

              <label for="nama_petugas"><b>Nama Petugas</b></label>
              <input type="text" value="" placeholder="" name="nama_petugas" required>

              <label for="email"><b>Email</b></label>
              <input type="text" placeholder="" value="" name="email" required>

            <div class="form-group">
                <label for="gambar"><b>Profil Petugas</b></label>
                <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp">
            </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button></a>
            </div>
            
          </form>

    <!-- AKHIR FORM petugas -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection