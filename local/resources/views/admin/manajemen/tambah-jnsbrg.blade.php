<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" rel="stylesheet" type="text/css" href="../../../../assets/css/admin.css">
    <link rel="stylesheet" href="../../../../assets/font/fontawesome-5/css/all.min.css">
    <link rel="stylesheet" href="../../../../assets/dataTables/datatables.css">

    <title>Inventoryst</title>
  </head>
  <body style="background-color:#E9E9E9;">
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">Welcome | Inventoryst</a>
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('jenisBrg')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH JENIS BARANG</h1>
            <p class="lead">Anda dapat mengolah data jenis barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->

    <form class="modal-content animate" action=""  style="width: 700px;margin-top:-100px;">
        <div class="container-start anda mt-2">
          <!-- Silakan Isi Data Anda . -->
        </div>

        <div class="container container-form">
            <div class="container-start">
                <h5 style="text-align:center;background-color:#fff;"><i class="far fa-plus-square"></i> TAMBAH JENIS BARANG</h5>
            </div>

          <label for="id_barang" class="mt-5"><b>ID Jenis Barang</b></label>
          <input type="text" placeholder="" name="id_barang" required>
          <br>

          <label for="jns_barang"><b>Jenis Barang</b></label>
          <input type="text" placeholder="" name="jns_barang" required>
        
            <label for="kode_jenis"><b>Kode Jenis Barang</b></label>
            <input type="text" placeholder="" name="kode_jenis" required>

        <label for="keterangan_jnsbarang"><b>Keterangan Jenis Barang</b></label>
        <textarea class="form-control" id="keterangan_jnsbarang" rows="3"></textarea>

          <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Edit</button></a>
        </div>

        <div class="container" style="background-color:#f1f1f1">
              <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">X</button>
      </div>
        
      </form>

    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../../../../assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="../../../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../../../assets/js/popper.min.js"></script>
    <script src="../../../../assets/js/bootstrap.min.js"></script>
    <script src="../../../../assets/dataTables/datatables.js"></script>


    <script type="text/javascript" src="../../../../assets/js/admin.js"></script>
  </body>
</html>