@extends('layouts.master')
@section('content')
        <div class="row ml-auto">
            <div class="col-lg">
                <div class="icon ml-3">
                    <h5>
                        <a href="{{route('peminjaman')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                        </a>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL PEMINJAMAN</h1>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

      <!-- TABLE PEMINJAMAN -->
    <div class="row">
        <div class="col-sm-4">
            <div class="container"></div>
        </div>
        <div class="col-md-10 vbrg-vw" style="margin-left: 100px;">
        <table class="table shadow-sm p-3 mb-5 bg-white rounded">
			<center>
            <thead class="thead-dark">
			<tr>
				<th></th>
				<th>Struk Peminjaman</th>
				<th></th>
			</tr>

			<tr>
				<td>Nomor : </td>
				<td></td>
				<td>{{$detailPj->id}}</td>
			</tr>

			<tr>
				<td>Nama Pegawai : </td>
				<td></td>
				<td>{{$detailPj->nama_pegawai}}</td>
			</tr>

            <tr>
				<td>ID Barang : </td>
				<td></td>
				<td>{{$detailPj->id_barang}}</td>
			</tr>

            <tr>
				<td>Tgl Pinjam : </td>
				<td></td>
				<td>{{$detailPj->tgl_pinjam}}</td>
			</tr>

            <tr>
				<td>Tgl Kembali : </td>
				<td></td>
				<td>{{$detailPj->tgl_kembali}}</td>
			</tr>
        </thead>
			</center>
		</table>
        </div>
    </div>
        <!-- AKHIR TABLE PEMINJAMAN -->

@endsection