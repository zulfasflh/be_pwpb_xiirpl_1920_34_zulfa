@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('barang')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
  </div>
</nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">EDIT BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->

    <form class="modal-content animate" action="{{route('updateBrg',$dataBr->id)}}" method="POST"  enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>EDIT BARANG</h5>
                </div>

              <label class="mt-3" for="nama"><b>Nama Barang</b></label>
              <input type="text" placeholder="" name="nama" value="{{$dataBr->nama}}" required>
              <br>

              <label for="nama_ruang"><b>Ruangan</b></label>
              <input type="text" placeholder="" name="nama_ruang" value="{{$dataBr->nama_ruang}}" required>
              <br>

              <label for="nama_jenis"><b>Jenis</b></label>
              <input type="text" placeholder="" name="nama_jenis" value="{{$dataBr->nama_jenis}}" required>
              <br>

                <label for="kondisi"><b>Kondisi Barang</b></label>
                <input type="text" placeholder="" name="kondisi" value="{{$dataBr->kondisi}}" required>
  
              <label for="keterangan"><b>Keterangan Barang</b></label>
              <textarea class="form-control" name="keterangan" id="keterangan" value="" rows="3">{{$dataBr->keterangan}}</textarea>
  
                <label for="jumlah"><b>Jumlah Barang</b></label>
                <input type="text" placeholder="" name="jumlah" value="{{$dataBr->jumlah}}" required>
                
                <label for="updated_at"><b>Tanggal Register</b></label>
                <input type="text" placeholder="" name="updated_at" value="{{$dataBr->updated_at}}" required>

                <div class="form-group">
                <label for="gambar"><b>Gambar</b></label>
                <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp">
                </div>

              <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Submit</button></a>
            </div>

            
          </form>

    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection