@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('barang')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate modal-vw" action="{{route('createBrg')}}" method="POST" enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>TAMBAH BARANG</h5>
                </div>

            <label for="kd_barang" class="mt-4"><b>Kode Barang</b></label>
              <input type="text" placeholder="" name="kd_barang" required>

              <label class="mt-2" for="nama_petugas"><b>Nama Petugas</b></label>
                <input class="form-control" type="text" placeholder="" name="nama_petugas" style="height:50px;" value="{{ auth()->user()->username }}" readonly>

              <label for="nama_ruang" class="mt-2"><b>Nama Ruang</b></label>
              <select name="nama_ruang" class="custom-select" id="inputGroupSelect04" style="height:50px;border-radius:0%;">
                    <!-- <option selected>Pilih...</option> -->
                    @foreach ($dataRuang as $r)
                    <option value="{{ $r->nama_ruang }}">{{ $r->nama_ruang }}</option>
                    @endforeach
              </select>
              <br>

              <label for="nama_jenis" class="mt-2"><b>Nama Jenis</b></label>
              <select id="nama_jenis" name="nama_jenis" class="custom-select" id="inputGroupSelect04" style="height:50px;border-radius:0%;">
                    <!-- <option selected>Pilih...</option> -->
                    @foreach ($dataJenis as $j)
                    <option value="{{ $j->nama_jenis }}">{{ $j->nama_jenis }}</option>
                    @endforeach
              </select>
              <br>

              <label for="nama" class="mt-2"><b>Nama Barang</b></label>
              <input type="text" placeholder="" name="nama" required>



              <div class="form-group mt-4">
                <label for="sel1">Kondisi Barang:</label>
                <select name="kondisi" class="form-control mt-1" id="sel1">
                    <option>Bisa Dipinjam</option>
                    <option>Rusak</option>
                </select>
                </div>

              <label for="jumlah" class="mt-2"><b>Jumlah Barang</b></label>
              <input type="text" placeholder="" name="jumlah" required>

              <label for="keterangan" class="mt-2"><b>Keterangan</b></label>
              <input type="text" placeholder="Tambah keterangan anda ..." name="keterangan" required>

                <div class="form-group">
                    <label for="gambar" class="mt-2"><b>Gambar Barang</b></label>
                    <input name="gambar" type="file" class="form-control-file btn btn-outline-dark" id="gambar" aria-describedby="fileHelp">
                </div>

              <button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button>
            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection