@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('dashboardall')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
  </div>
</nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">DETAIL BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate modal-vw" action="">
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>DETAIL BARANG</h5>
                </div>

                <div class="col-lg">
                    <img src="{{$dataBr->getGambar()}}" alt="..." class="img-thumbnail mx-auto d-block mt-3" style="width:200px;">
                </div>

                <label for="nama"><b>Nama Barang</b></label>
                <input type="text" placeholder="" name="nama" value="{{$dataBr->nama}}" readonly>

                <label for="nama_jenis"><b>Jenis Barang</b></label>
                <input type="text" placeholder="" name="nama_jenis" value="{{$dataBr->nama_jenis}}" readonly>

                <label for="nama_ruang"><b>Ruangan</b></label>
                <input type="text" placeholder="" name="nama_ruang" value="{{$dataRuang->first()->nama_ruang}}" readonly>
  
                <label for="kondisi"><b>Kondisi Barang</b></label>
                <input type="text" placeholder="" name="kondisi" value="{{$dataBr->kondisi}}" readonly>

                <label for="keterangan"><b>Keterangan Barang</b></label>
                <input type="text" placeholder="" name="keterangan" value="{{$dataBr->keterangan}}" readonly>
  
                <label for="jumlah"><b>Jumlah Barang</b></label>
                <input type="text" placeholder="" name="jumlah" value="{{$dataBr->jumlah}}" readonly>

                <label for="updated_at"><b>Tanggal Register</b></label>
                <input type="text" placeholder="" name="updated_at" value="{{$dataBr->updated_at}}" readonly>

                <!-- <a href="{{route('barangexportxls')}}"><button class="btn btn-outline-dark"><i class="fas fa-file-excel"></i> Export Excel </button></a>
                <a href="{{route('barangexportpdf')}}"><button class="btn btn-outline-dark"><i class="fas fa-file-pdf"></i> Export PDF </button></a> -->
          
            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection