@extends('layouts.master')
@section('content')
          
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('barang')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->
    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">TAMBAH BARANG</h1>
            <p class="lead">Anda dapat mengolah data barang di Inventoryt .</p>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate modal-vw" action="{{route('createStok')}}" method="POST" enctype="multipart/form-data">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>TAMBAH STOK {{($x->nama)}}</h5>
                </div>

              <label for="id_barang" class="mt-4"><b>Kode Barang</b></label>
              <input type="text" placeholder="" id="id_barang" name="id_barang" value="{{ $x->id }}" readonly>

              <label for="jumlah" class="mt-2"><b>Jumlah Barang</b></label>
              <input type="text" placeholder="" name="jumlah" class=" mb-3" required>
              <br>

              <button type="submit" class="btn btn-outline-dark" style="height:50px;">Tambah</button>
            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

@endsection