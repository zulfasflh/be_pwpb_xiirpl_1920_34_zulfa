@extends('layouts.master')
@section('content')
        <div class="row ml-auto">
        <div class="col-lg">
            <div class="icon ml-3">
                <h5>
                    <a href="{{route('peminjaman')}}"> <button class="btn btn-outline-dark"><i class="fas fa-times"></i></button></i>
                    </a>
                </h5>
            </div>
        </div>
    </div>
        </div>
      </nav>
    <!-- AKHIR NAVBAR -->

    <!-- JUMBOTRON -->

    <div class="jumbotron jumbotron-fluid" style="background-color: transparent;">
        <div class="container mt-5">
            <h1 class="display-4">EDIT STATUS</h1>
            <hr>
        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->

    <!-- FORM BARANG -->
    <form class="modal-content animate" action="{{route('createStatus')}}" method="POST">
    @csrf
            <div class="container-start anda mt-2">
              <!-- Silakan Isi Data Anda . -->
            </div>

            <div class="container container-form">
                <div class="container-start container-vw">
                    <h5>Verifikasi Peminjaman</h5>
                </div>

                <div class="form-group mt-4">
                <label for="id_peminjaman" class="mt-4"><b>Kode Peminjaman</b></label>
                <input type="text" placeholder="" id="id_peminjaman" name="id_peminjaman" value="{{ $x->id }}" readonly>

                <label for="status_pinjam"><b>Status Pinjam</b></label>
                <select class="form-control mt-1" name="status_pinjam" id="status_pinjam">
                    <option>Diverifikasi</option>
                    <option>Antrian</option>
                </select>
                </div>

                <a href="#"><button type="submit" class="btn btn-outline-dark" style="height:50px;">Submit</button></a>
  
            </div>
            
          </form>
    <!-- AKHIR FORM BARANG -->

        </div>
    </div>
    <!-- AKHIR JUMBOTRON -->
@endsection