<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER tr_pjBarang AFTER INSERT ON `peminjaman` FOR EACH ROW
            BEGIN
                INSERT INTO detail_pinjam (`id`, `id_barang`, `jumlah`) 
                VALUES (3, NEW.id, now(), null);
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_pjBarang`');
    }
}
    