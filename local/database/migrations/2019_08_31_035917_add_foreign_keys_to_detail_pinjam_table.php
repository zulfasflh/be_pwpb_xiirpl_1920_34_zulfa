<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetailPinjamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detail_pinjam', function(Blueprint $table)
		{
			$table->foreign('id_barang', 'detail_pinjam_ibfk_1')->references('id_barang')->on('barang')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detail_pinjam', function(Blueprint $table)
		{
			$table->dropForeign('detail_pinjam_ibfk_1');
		});
	}

}
