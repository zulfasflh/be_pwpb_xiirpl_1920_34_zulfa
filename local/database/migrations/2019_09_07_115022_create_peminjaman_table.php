<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeminjamanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peminjaman', function(Blueprint $table)
		{
			$table->char('id_peminjaman', 10);
			$table->string('id_dtlpinjam');
			$table->char('nip', 10);
			$table->date('tgl_pinjam');
			$table->date('tgl_kembali');
			$table->string('status_pinjam', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('peminjaman');
	}

}
