<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barang', function(Blueprint $table)
		{
			$table->char('id_barang', 10)->primary();
			$table->char('id_ptg', 10)->nullable()->index('id_ptg');
			$table->char('id_ruang', 10)->index('id_ruang');
			$table->char('id_jenis', 10)->index('id_jenis');
			$table->string('nama', 50);
			$table->string('kondisi', 50);
			$table->text('keterangan', 65535)->nullable();
			$table->integer('jumlah');
			$table->date('tgl_register')->nullable();
			$table->char('kode_barang', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('barang');
	}

}
