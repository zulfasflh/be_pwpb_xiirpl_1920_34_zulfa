<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRuangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ruang', function(Blueprint $table)
		{
			$table->char('id_ruang', 10)->primary();
			$table->string('nama_ruang', 50);
			$table->string('kode_ruang', 10);
			$table->text('keterangan', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ruang');
	}

}
