<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBarangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('barang', function(Blueprint $table)
		{
			$table->foreign('id_ptg', 'barang_ibfk_1')->references('id_ptg')->on('petugas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_ruang', 'barang_ibfk_2')->references('id_ruang')->on('ruang')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_jenis', 'barang_ibfk_3')->references('id_jenis')->on('jenis')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('barang', function(Blueprint $table)
		{
			$table->dropForeign('barang_ibfk_1');
			$table->dropForeign('barang_ibfk_2');
			$table->dropForeign('barang_ibfk_3');
		});
	}

}
