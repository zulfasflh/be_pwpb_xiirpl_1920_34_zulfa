<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePengembalianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengembalian', function(Blueprint $table)
		{
			$table->char('id_pengembalian', 10);
			$table->char('id_peminjaman', 10);
			$table->char('id_pgw', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pengembalian');
	}

}
