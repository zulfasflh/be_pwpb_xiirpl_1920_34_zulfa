<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailPinjamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void	
	 */
	public function up()
	{
		Schema::create('detail_pinjam', function(Blueprint $table)
		{
			$table->char('id_dtlPj', 10);
			$table->char('id_barang', 10);
			$table->integer('jumlah')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_pinjam');
	}

}
