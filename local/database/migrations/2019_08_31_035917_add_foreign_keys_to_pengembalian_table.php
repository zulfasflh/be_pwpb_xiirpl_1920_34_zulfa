<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPengembalianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pengembalian', function(Blueprint $table)
		{
			$table->foreign('id_peminjaman', 'pengembalian_ibfk_1')->references('id_peminjaman')->on('peminjaman')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('id_pgw', 'pengembalian_ibfk_2')->references('id_pgw')->on('pegawai')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pengembalian', function(Blueprint $table)
		{
			$table->dropForeign('pengembalian_ibfk_1');
			$table->dropForeign('pengembalian_ibfk_2');
		});
	}

}
