<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailPinjamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail_pinjam', function(Blueprint $table)
		{
			$table->char('id_dtlpinjam', 10)->primary();
			$table->char('id_barang', 10)->index('id_barang');
			$table->integer('jumlah')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_pinjam');
	}

}
