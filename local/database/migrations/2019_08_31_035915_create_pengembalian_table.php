<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePengembalianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengembalian', function(Blueprint $table)
		{
			$table->char('id_pengembalian', 10)->primary();
			$table->char('id_peminjaman', 10)->index('id_peminjaman');
			$table->char('id_pgw', 10)->index('id_pgw');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pengembalian');
	}

}
