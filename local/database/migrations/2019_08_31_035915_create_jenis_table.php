<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJenisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jenis', function(Blueprint $table)
		{
			$table->char('id_jenis', 10)->primary();
			$table->string('nama_jenis', 50);
			$table->string('kode_jenis', 10);
			$table->text('keterangan', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jenis');
	}

}
