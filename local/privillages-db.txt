CREATE USER '19xiirpl34_admin'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.barang TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.ruang TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.jenis TO
'19xiirpl34_admin'@'localhost';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.peminjaman TO
'19xiirpl34_admin'@'localhost';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.pengembalian TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.users TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.petugas TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.pegawai TO
'19xiirpl34_admin'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.migrations TO
'19xiirpl34_admin'@'localhost';
GRANT SELECT, INSERT ON 1920_xiirpl_3_34_zulfa.stok TO
'19xiirpl34_admin'@'localhost';
GRANT SELECT, INSERT ON 1920_xiirpl_3_34_zulfa.status TO
'19xiirpl34_admin'@'localhost';
GRANT SELECT, INSERT ON 1920_xiirpl_3_34_zulfa.detail_pinjam TO
'19xiirpl34_admin'@'localhost';

CREATE USER '19xiirpl34_op'@'localhost' IDENTIFIED BY 'operator';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.migrations TO
'19xiirpl34_op'@'localhost';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.barang TO
'19xiirpl34_op'@'localhost';
GRANT ALL ON 1920_xiirpl_3_34_zulfa.pegawai TO
'19xiirpl34_op'@'localhost';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.peminjaman TO
'19xiirpl34_op'@'localhost';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.pengembalian TO
'19xiirpl34_op'@'localhost';

CREATE USER '19xiirpl34_pgw'@'localhost' IDENTIFIED BY 'pegawai';
GRANT SELECT ON 1920_xiirpl_3_34_zulfa.barang TO
'19xiirpl34_pgw'@'localhost';
GRANT SELECT,INSERT ON 1920_xiirpl_3_34_zulfa.peminjaman TO
'19xiirpl34_pgw'@'localhost';


==== TAMBAHAN ====
GRANT INSERT ON 1920_xiirpl_3_34_zulfa.peminjaman TO
'19xiirpl34_admin'@'localhost';
GRANT INSERT ON 1920_xiirpl_3_34_zulfa.peminjaman TO
'19xiirpl34_pgw'@'localhost';